#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define N_EQ1		2
#define NP1		8
#define NUM_THREADS_PER_BLOCK 512

#define COUPLING_THRESHOLD	0. //
#define THRESHOLD_SLOPE		100. //

__device__ double boltzmann(const double V, const double V_0, const double k)
{
	return 1./(1.+exp(-k*(V-V_0)));
}

__device__ void derivs_one_electricExcitatoryAndInhibitoryConnections(double* y, double* dxdt, const double* p)
{
	dxdt[0] = p[7]*(y[0]-y[0]*y[0]*y[0])-y[1]+p[0]; // x' = m (x-x^3)-y+I
	dxdt[1] = p[1]*(boltzmann(y[0], p[2], p[3])-y[1]); // y' = eps * (Bfun(x, x_0, k_2) - y)
}

/*
Computes the following derivatives for each neuron and also adds the input from other neurons :
     x' = m (x-x^3)-y+I
     y' = eps * (Bfun(x, x_0, k_2) - y)
         n => number of neurons
         y => array with structure x0,y0,x1,y1,x2,y2,...x{n-1},y{n-1} for the above equations
         dydt => derivatives of y i.e., x0',y0',x1',y1',x2',y2'...x{n-1}',y{n-1}'
         p => parameter array storing NP1 parameters for each neuron i.e., p[0] to p[NP1-1] for x0',y0' followed by
                                p[NP] to p[2*NP-1] for x1',y1' and so on
         coupling_strengths_type => array of size n*n storing coupling strengths in the order (0->0),(0->1),(0->2) ... (0->n-1) followed by
                              (1->0),(1->1),(1->2),(1->3)...(1->n-1) and so on

     xi'_coupled = xi' + (E0_chlorine - xi) * ( coupling_strengths_chlorine(0->i)*bm_factor(x0) + coupling_strength_chlorine(1->i)*bm_factor(x1) + ... )
                    + (E0_potassium - xi) * ( coupling_strengths_potassium(0->i)*bm_factor(x0) + coupling_strength_potassium(1->i)*bm_factor(x1) + ... )
                    + (E0_sodium - xi) * ( coupling_strengths_sodium(0->i)*bm_factor(x0) + coupling_strength_sodium(1->i)*bm_factor(x1) + ... )
                    + (x0 - xi) * coupling_strengths_chlorine(0->i) + (x1 - xi) * coupling_strengths_chlorine(1->i) + ... ;
     yi'_coupled = yi'
     bm_factor here uses different k, x0 than in the computation of uncoupled y' ;
     A neuron j will influence neuron i only if it's voltage is above the threshold obtained from the boltzmann function; See bolzmann.py
     TODO : Instead of using three different arrays, should use a linked list for the coupling strengths; For each neuron 'i', the correspoinding linked list will contain nodes each having the presynaptic neuron number, synaptic strenght, and type of channel (or the channel's equilibrium potential); This improves performance from O(n^2) to O(E) where E is no.of edges or synaptic connections
*/
__device__ void derivs_n_variablecoupling_electricExcitatoryAndInhibitoryConnections(const unsigned n, double* y, double* dydt,
                                const double* p, const double* coupling_strengths_chlorine, const double* coupling_strengths_potassium,
                                const double* coupling_strengths_sodium, const double* coupling_strengths_electric,
                                double* bm_factor)
{
	unsigned i, j;

	for(i=0;i <n; i++){
	    bm_factor[i] = boltzmann(y[i*N_EQ1], COUPLING_THRESHOLD, THRESHOLD_SLOPE);
		derivs_one_electricExcitatoryAndInhibitoryConnections(y+i*N_EQ1, dydt+i*N_EQ1, p+i*NP1);
	}
	for(i=0; i<n; i++)
	{
		for(j=0;j<n;j++){
		//TODO : p[4] is E_Cl; p[5] is E_K; p[6] is E_Na;
		   dydt[i*N_EQ1] += ((p[4+i*NP1]-y[i*N_EQ1])*coupling_strengths_chlorine[j*n+i] +
		                        (p[5+i*NP1]-y[i*N_EQ1])*coupling_strengths_potassium[j*n+i] +
		                            (p[6+i*NP1]-y[i*N_EQ1])*coupling_strengths_sodium[j*n+i] )
		                             * bm_factor[j]
                             + (y[j*N_EQ1]-y[i*N_EQ1])*coupling_strengths_electric[j*n+i] ;
		}
	}
}


/*
    This method finds the rk4 approximation of the curve for a system of 'n' neurons obeying the following equations :
         x' = m (x-x^3)-y+I
         y' = eps * (Bfun(x, x_0, k_2) - y)
     networkSize => number of neurons
     y => array with structure x0,y0,x1,y1,x2,y2,...x{n-1},y{n-1} for the above equations
     dydt => derivatives of y i.e., x0',y0',x1',y1',x2',y2'...x{n-1}',y{n-1}'
     p => parameter array storing NP1 parameters for each neuron i.e., p[0] to p[NP1-1] for x0',y0' followed by
                            p[NP] to p[2*NP-1] for x1',y1' and so on
     coupling_strengths => array of size n*n storing coupling strengths in the order (0->0),(0->1),(0->2) ... (0->n-1) followed by
                          (1->0),(1->1),(1->2),(1->3)...(1->n-1) and so on
     output => array of size n*N in the following order x0,x1,...x{n-1} representing initial point on the curve
                        followed by x0-1,x1-1,...x{n-1}-1 representing the next point and so on
     dt => step size
     N => number of points to compute
     stride => number of strides to make while computing each of the N points

     also in the output for each time point 1 to N, check crossings for each neuron;
     for each neuron store all the crossings and also the number of crossings present;
     compute and return phase differences of all cells w.r.t the first cell phase differences
*/
/*
    Same as integrate_n_rk4_phasedifferences_electricExcitatoryAndInhibitoryConnections
    No need to return output array, so uses a cyclic array for output
*/
__device__ void integrate_n_rk4_phasedifferences_nooutput_electricExcitatoryAndInhibitoryConnections(const unsigned networkSize,
                    const double* initial_state, const double* p,
                    const double* coupling_strengths_chlorine, const double* coupling_strengths_potassium,
                    const double* coupling_strengths_sodium, const double* coupling_strengths_electric,
		    double* trajectory_targets_phasedifferences,
		    double *output,double *phase_differences_output,double *current_state,double *temp_state,double *k1,double *k2,double *k3,double *k4,double *bm_factor,
		    int *lastCrossings, int *numberOfCrossingsToEdit,
                    const double dt, const unsigned N, const unsigned stride, const unsigned cycles, const unsigned initial_states_start, const unsigned trajectory_targets_phasedifferences_start) {

    unsigned i, j, m, point_dimension = networkSize*N_EQ1;
	int threshhold = 0, allCellsHadZeroPhaseBefore = 0, numberOfCrossingsUnedited = 0, phase_differences_output_index=0;
	unsigned numberOfPhaseDifferences=0;

	for(m=0; m<point_dimension; m++){
	    current_state[m]=initial_state[m+initial_states_start];
	    if(m%2==0) {
	        output[m/2]=current_state[m];
	        lastCrossings[m/2]=N+1;
            numberOfCrossingsToEdit[m/2] = 0;
	    }
	}

	for(i=1; i<N; i++)
	{
		for(j=0; j<stride; j++)
		{
			derivs_n_variablecoupling_electricExcitatoryAndInhibitoryConnections(networkSize, current_state, k1, p, coupling_strengths_chlorine, coupling_strengths_potassium, coupling_strengths_sodium, coupling_strengths_electric, bm_factor);

			for(m=0; m<point_dimension; m++)
				temp_state[m] = current_state[m]+k1[m]*dt/2;
			derivs_n_variablecoupling_electricExcitatoryAndInhibitoryConnections(networkSize, temp_state, k2, p, coupling_strengths_chlorine, coupling_strengths_potassium, coupling_strengths_sodium, coupling_strengths_electric,bm_factor);

			for(m=0; m<point_dimension; m++)
				temp_state[m] = current_state[m]+k2[m]*dt/2;
			derivs_n_variablecoupling_electricExcitatoryAndInhibitoryConnections(networkSize, temp_state, k3, p, coupling_strengths_chlorine, coupling_strengths_potassium, coupling_strengths_sodium, coupling_strengths_electric, bm_factor);

			for(m=0; m<point_dimension; m++)
				temp_state[m] = current_state[m]+k3[m]*dt;
			derivs_n_variablecoupling_electricExcitatoryAndInhibitoryConnections(networkSize, temp_state, k4, p, coupling_strengths_chlorine, coupling_strengths_potassium, coupling_strengths_sodium, coupling_strengths_electric, bm_factor);

			for(m=0; m<point_dimension; m++)
				current_state[m] += (k1[m]+2.*(k2[m]+k3[m])+k4[m])*dt/6;
		}
		for(m=0; m<point_dimension; m++){
            if(m%2==0) {
                //Outputs only voltages of each cell.
                output[(i%2)*point_dimension/2+m/2]=current_state[m];

                // computing the crossings here itself
                if(m==0){ // first cell
                    if(current_state[m]>=threshhold && output[((i-1)%2)*point_dimension/2+m/2] < threshhold){
                        //implies a crossing in the first cell
                        if(!allCellsHadZeroPhaseBefore){
                            int allCellsHadZeroPhaseBefore1 = 1;
                            for(j=1;j<point_dimension/2;j++){
                                if(i<lastCrossings[j]) {
                                    allCellsHadZeroPhaseBefore1=0;
                                }
                            }
                            allCellsHadZeroPhaseBefore = allCellsHadZeroPhaseBefore1;
                        }
                        if(allCellsHadZeroPhaseBefore){
                            for(j=1;j<point_dimension/2;j++){
                                phase_differences_output[phase_differences_output_index*(point_dimension/2 -1) + j-1] = i - lastCrossings[j];
                                numberOfCrossingsToEdit[j]=numberOfCrossingsToEdit[j]+1;
                            }
                            phase_differences_output_index++;
                        }
                    }
                } else{ // cells other than first cell
                    if(current_state[m]>=threshhold && output[((i-1)%2)*point_dimension/2+m/2] < threshhold){
                        if(!allCellsHadZeroPhaseBefore){
                            lastCrossings[m/2]=i;
                        }else{
                            j=numberOfCrossingsToEdit[m/2];
                            while(j>0){
                                phase_differences_output[(phase_differences_output_index-j)*(point_dimension/2-1)+m/2-1]/=i-lastCrossings[m/2];
                                j--;
                            }
                            lastCrossings[m/2]=i;
                            numberOfCrossingsToEdit[m/2]=0;
                        }
                    }
                }

            }
		}
	}

    for(m=0; m<point_dimension; m++){
	    if(m%2==0) {
            if(numberOfCrossingsToEdit[m/2]>numberOfCrossingsUnedited){
                numberOfCrossingsUnedited = numberOfCrossingsToEdit[m/2];
            }
	    }
	}
    //number of the phase differences in the array
    numberOfPhaseDifferences = phase_differences_output_index - numberOfCrossingsUnedited;

	for(m=0;m<networkSize-1;m++){
	   if(numberOfPhaseDifferences>0) {
	    	trajectory_targets_phasedifferences[trajectory_targets_phasedifferences_start+m]=
		    phase_differences_output[(numberOfPhaseDifferences-1)*(networkSize-1)+m];
	    }
	}

}


/*
for an array of initial conditions, do everything in integrate_n_rk4_phasedifferences and return just the last point in phase differences trajectory for each initial condition
*/
__global__ void sweeptracesthreads_electricExcitatoryAndInhibitoryConnections(const long networkSize, const double* initial_states_array, const double* p,
                    const double* coupling_strengths_chlorine, const double* coupling_strengths_potassium,
                    const double* coupling_strengths_sodium, const double* coupling_strengths_electric,
                    double* trajectory_targets_phasedifferences,
		    double *output,double *phase_differences_output,double *current_state,double *temp_state,double *k1,double *k2,double *k3,double *k4,double *bm_factor,
		    int *lastCrossings, int *numberOfCrossingsToEdit,
                    const long noOfInitialStates, const double dt, const long N, const int cycles, const long stride) {

    int tx=blockIdx.x * blockDim.x + threadIdx.x, point_dimension = networkSize*N_EQ1;

	if(tx<noOfInitialStates){
		integrate_n_rk4_phasedifferences_nooutput_electricExcitatoryAndInhibitoryConnections(networkSize,initial_states_array, p,
		            coupling_strengths_chlorine, coupling_strengths_potassium, coupling_strengths_sodium, coupling_strengths_electric, trajectory_targets_phasedifferences, 
		    output+tx*2*networkSize,phase_differences_output+tx*(networkSize-1)*(cycles),current_state+tx*point_dimension,temp_state+tx*point_dimension,k1+tx*point_dimension, k2+tx*point_dimension,k3+tx*point_dimension,k4+tx*point_dimension,bm_factor+tx*networkSize,
		    lastCrossings+tx*point_dimension/2, numberOfCrossingsToEdit+tx*point_dimension/2,
			dt, N, cycles, stride, tx*point_dimension, tx*(networkSize-1));
	}

}

/**
 * error checking routine
 */
void checkAndDisplayErrors(char *label)
{
  // we need to synchronise first to catch errors due to
  // asynchronous operations that would otherwise
  // potentially go unnoticed

  cudaError_t err;

  err = cudaThreadSynchronize();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }

  err = cudaGetLastError();
  if (err != cudaSuccess)
  {
    char *e = (char*) cudaGetErrorString(err);
    fprintf(stderr, "CUDA Error: %s (at %s)", e, label);
  }
}

extern "C" {

    void sweeptraces_electricExcitatoryAndInhibitoryConnections(const long networkSize, const double* initial_states_array, const double* p,
                        const double* coupling_strengths_chlorine, const double* coupling_strengths_potassium,
                        const double* coupling_strengths_sodium, const double* coupling_strengths_electric,
                        double* trajectory_targets_phasedifferences,
                        const long noOfInitialStates, const double dt, const long N, const int cycles, const long stride) {


/*
    #pragma acc parallel loop independent ,\
        pcopyin(initial_states_array[0:point_dimension*noOfInitialStates],p[0:NP1*networkSize],coupling_strengths_chlorine[0:networkSize*networkSize],coupling_strengths_potassium[0:networkSize*networkSize],coupling_strengths_sodium[0:networkSize*networkSize],coupling_strengths_electric[0:networkSize*networkSize]), \
        copy(trajectory_targets_phasedifferences[0:(networkSize-1)*noOfInitialStates]),
*/
	    int point_dimension = networkSize*N_EQ1;

            /*allocate device memory. */
            double* initial_states_array_gpu, *p_gpu, *coupling_strengths_chlorine_gpu, *coupling_strengths_potassium_gpu, *coupling_strengths_sodium_gpu, *coupling_strengths_electric_gpu;
	    double *trajectory_targets_phasedifferences_gpu;
            (cudaMalloc( (void**) &initial_states_array_gpu, N_EQ1*networkSize*noOfInitialStates*sizeof(double)));
            (cudaMalloc( (void**) &p_gpu, NP1*networkSize*sizeof(double)));
            (cudaMalloc( (void**) &coupling_strengths_chlorine_gpu, networkSize*networkSize*sizeof(double)));
            (cudaMalloc( (void**) &coupling_strengths_potassium_gpu, networkSize*networkSize*sizeof(double)));
            (cudaMalloc( (void**) &coupling_strengths_sodium_gpu, networkSize*networkSize*sizeof(double)));
            (cudaMalloc( (void**) &coupling_strengths_electric_gpu, networkSize*networkSize*sizeof(double)));
            (cudaMalloc( (void**) &trajectory_targets_phasedifferences_gpu, (networkSize-1)*noOfInitialStates*sizeof(double)));

	    double *output, *phase_differences_output, *current_state, *temp_state, *k1, *k2, *k3, *k4, *bm_factor;
    	    int *lastCrossings, *numberOfCrossingsToEdit;	    
            (cudaMalloc( (void**) &output, noOfInitialStates*2*networkSize*sizeof(double)));
            (cudaMalloc( (void**) &phase_differences_output, noOfInitialStates*(networkSize-1)*(cycles)*sizeof(double)));
            (cudaMalloc( (void**) &current_state, noOfInitialStates*point_dimension*sizeof(double)));
            (cudaMalloc( (void**) &temp_state, noOfInitialStates*point_dimension*sizeof(double)));
            (cudaMalloc( (void**) &k1, noOfInitialStates*point_dimension*sizeof(double)));
            (cudaMalloc( (void**) &k2, noOfInitialStates*point_dimension*sizeof(double)));
            (cudaMalloc( (void**) &k3, noOfInitialStates*point_dimension*sizeof(double)));
            (cudaMalloc( (void**) &k4, noOfInitialStates*point_dimension*sizeof(double)));
            (cudaMalloc( (void**) &lastCrossings, noOfInitialStates*point_dimension/2*sizeof(int)));
            (cudaMalloc( (void**) &numberOfCrossingsToEdit, noOfInitialStates*point_dimension/2*sizeof(int)));
            (cudaMalloc( (void**) &bm_factor, noOfInitialStates*networkSize*sizeof(double)));

            checkAndDisplayErrors("Memory allocation");

            // copy from host to device
            cudaMemcpy(initial_states_array_gpu, initial_states_array, N_EQ1*networkSize*noOfInitialStates*sizeof(double), cudaMemcpyHostToDevice);
            cudaMemcpy(p_gpu ,p , NP1*networkSize*sizeof(double), cudaMemcpyHostToDevice);
            cudaMemcpy(coupling_strengths_chlorine_gpu , coupling_strengths_chlorine,networkSize*networkSize*sizeof(double) , cudaMemcpyHostToDevice);
            cudaMemcpy(coupling_strengths_potassium_gpu , coupling_strengths_potassium,networkSize*networkSize*sizeof(double) , cudaMemcpyHostToDevice);
            cudaMemcpy(coupling_strengths_sodium_gpu , coupling_strengths_sodium,networkSize*networkSize*sizeof(double) , cudaMemcpyHostToDevice);
            cudaMemcpy(coupling_strengths_electric_gpu , coupling_strengths_electric,networkSize*networkSize*sizeof(double) , cudaMemcpyHostToDevice);
            cudaMemcpy(trajectory_targets_phasedifferences_gpu, trajectory_targets_phasedifferences, (networkSize-1)*noOfInitialStates*sizeof(double), cudaMemcpyHostToDevice);
            checkAndDisplayErrors("Error while copying from host to device.");

            /*timing*/
            cudaEvent_t start_event, stop_event;
            cudaEventCreate(&start_event) ;
            cudaEventCreate(&stop_event) ;
            cudaEventRecord(start_event, 0);

            /*Dimensions. */

            int gridXDimension = noOfInitialStates/NUM_THREADS_PER_BLOCK;
            if(noOfInitialStates%NUM_THREADS_PER_BLOCK!=0) {
                    gridXDimension += 1;
            }
            dim3 dimGrid(gridXDimension,1);
            dim3 dimBlock(NUM_THREADS_PER_BLOCK,1);

            printf(" Num of blocks per grid:       %d\n", gridXDimension);
            printf(" Num of threads per block:     %d\n", NUM_THREADS_PER_BLOCK);
            printf(" Total Num of threads running: %d\n", gridXDimension*NUM_THREADS_PER_BLOCK);
            printf(" No.of initial states = %ld \n",noOfInitialStates);

            /*Call kernel(global function)*/
            sweeptracesthreads_electricExcitatoryAndInhibitoryConnections<<<dimGrid, dimBlock>>>(networkSize,initial_states_array_gpu, p_gpu, coupling_strengths_chlorine_gpu, coupling_strengths_potassium_gpu, coupling_strengths_sodium_gpu, coupling_strengths_electric_gpu, trajectory_targets_phasedifferences_gpu, 
		    output,phase_differences_output,current_state,temp_state,k1, k2,k3,k4,bm_factor,
		    lastCrossings, numberOfCrossingsToEdit,
noOfInitialStates, dt, N, cycles, stride);

		printf(" does this work?");

            cudaThreadSynchronize();
            cudaEventRecord(stop_event, 0);
            cudaEventSynchronize(stop_event);

            float time_kernel;
            cudaEventElapsedTime(&time_kernel, start_event, stop_event);
            printf("Total time(sec) %f\n", time_kernel/1000);

            /*copy data from device memory to memory. */
            cudaMemcpy(trajectory_targets_phasedifferences, trajectory_targets_phasedifferences_gpu, (networkSize-1)*noOfInitialStates*sizeof(double), cudaMemcpyDeviceToHost);
            checkAndDisplayErrors("Error while copying from device to host.");

            /*Free all allocated memory. */
            cudaFree(initial_states_array_gpu);
            cudaFree(p_gpu);
            cudaFree(coupling_strengths_chlorine_gpu);
            cudaFree(coupling_strengths_potassium_gpu);
            cudaFree(coupling_strengths_sodium_gpu);
            cudaFree(coupling_strengths_electric_gpu);
            cudaFree(trajectory_targets_phasedifferences_gpu);


    }

}

/*
do everything in sweeptraces_electricExcitatoryAndInhibitoryConnections but also compute whether the phase difference trajectories converge to fixed points :
cuda code needs to be updated from fithugh_allSynapseTypes.c
void sweeptraces_..._WithFixedPoints(...) {}
*/



