#!/usr/bin/env python

import time
import sys
import Network_Generic_allSynapseTypes as netw
import sweepClusters

sys.path.insert(0, '../Tools')
import tools as tl
import distribute
import window as win
import numpy as np
import fitzhugh_allSynapseTypes as fh

class torus_3(win.window):
    title = 'Phase Torus'
    figsize = (7, 6)

    def __init__(self, network, info=None, position=None):
        win.window.__init__(self, position)
        self.network = network
        self.info = info
        self.GRID = 10
        self.USE_GPU = False
        self.basin_image = None
        self.setupFigure()
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)

    def setupFigure(self):
        self.fig.clear()
        ticks = 0.1 * np.arange(11)

        self.ax_traces = self.fig.add_subplot(111, xticks=ticks, yticks=ticks[1:])
        self.ax_traces.set_xlabel(r'$\Delta\theta_{12}$', fontsize=20)
        self.ax_traces.set_ylabel(r'$\Delta\theta_{13}$', fontsize=20)
        self.ax_traces.set_xlim(0., 1.)
        self.ax_traces.set_ylim(0., 1.)

    def on_click(self, event):
        self.x=event.xdata; self.y = event.ydata;
        self.click_traces_efficient(self.x, self.y, plotit=True);

    def click_traces_efficient(self, cell2Phase, cell3Phase, plotit = True):

        initial_condition = self.network.load_initial_condition_generic([0, cell2Phase, cell3Phase] )

        V,d = fh.integrate_n_rk4_phasedifferences_electricExcitatoryAndInhibitoryConnections(self.network.networkSize,
            initial_condition,
            self.network.coupling_strengths_chlorine,
            self.network.coupling_strengths_potassium,
            self.network.coupling_strengths_sodium,
            self.network.coupling_strengths_electric,
            self.network.systemList[0].dt / float(self.network.systemList[0].stride),
            self.network.systemList[0].N_output(self.network.CYCLES),
            self.network.systemList[0].stride, self.network.parameterSet);

        if plotit:
            tl.plot_phase_2D(d[:, 0], d[:, 1], axes=self.ax_traces, c=tl.clmap(tl.PI2 * d[-1,1], tl.PI2 * d[-1,0]), PI=0.5)
            self.fig.canvas.draw()
            return

        return d

    def sweepTorus(self,useGPU=False, sweepResolution=3):
        self.setupFigure()
        if useGPU == True :
            print("Running C GPU Method")
            self.sweepGPUInC(sweepResolution)
        else :
            print("Running python parallel processing Method")
            self.sweepCPUInPython(sweepResolution)

    def sweepCPUInPython(self,sweepResolution=3):
        startTime = time.time();
        sweepResolutionFloat = float(sweepResolution);
        x = distribute.distribute(self.click_traces_efficient_wrapper,'choice',range(sweepResolution*sweepResolution),
                              kwargs={'sweepResolution':sweepResolution})

        for i in range(sweepResolution*sweepResolution):
            self.ax_traces.plot((i/sweepResolution)/sweepResolutionFloat, (i%sweepResolution)/sweepResolutionFloat, 'o', c=tl.clmap(tl.PI2*x[i][-1,1],tl.PI2*x[i][-1,0]))

        self.fig.canvas.draw()
        endTime = time.time();
        print("time taken in seconds for sweep :", endTime - startTime);

        return

    def sweepGPUInC(self,sweepResolution=5):
        sweepResolutionFloat = float(sweepResolution);
        initial_condition_array=[]
        for i in range(sweepResolution):
            for j in range(sweepResolution):
                        initial_condition = self.network.load_initial_condition_generic([0, i/sweepResolutionFloat,
                                                                                        j/sweepResolutionFloat] )
                        initial_condition_array.extend(initial_condition)

        startTime = time.time();
        #no.of cycles is based on last neuron
        d, p, noP = fh.sweeptraces_electricExcitatoryAndInhibitoryConnections(self.network.networkSize,initial_condition_array,
                                                                      sweepResolution*sweepResolution,
                                                                      self.network.coupling_strengths_chlorine,
                                                                      self.network.coupling_strengths_potassium,
                                                                      self.network.coupling_strengths_sodium,
                                                                      self.network.coupling_strengths_electric,
                                                                      self.network.systemList[0].dt/
                                                                        float(self.network.systemList[0].stride),
                                                                      self.network.systemList[0].N_output(self.network.CYCLES),
                                                                      self.network.CYCLES,
                                                                      self.network.systemList[0].stride,
                                                                      self.network.parameterSet, withFixedPointDetection=True)

        sweepClusters.clusterize(d);

        for i in range(sweepResolution):
            for j in range(sweepResolution):
                self.ax_traces.plot(i/sweepResolutionFloat, j/sweepResolutionFloat, 'o', c=tl.clmap(tl.PI2 * (d[i*sweepResolution+j,1]), tl.PI2 * ((d[i*sweepResolution+j,0]))))

        self.fig.canvas.draw()
        endTime = time.time();
        print("time taken in seconds for sweep :", endTime - startTime);
        return

    def click_traces_efficient_wrapper(self,choice,sweepResolution):
        #print (choice/sweepResolution)/float(sweepResolution),(choice%sweepResolution)/float(sweepResolution)
        return self.click_traces_efficient((choice/sweepResolution)/float(sweepResolution),(choice%sweepResolution)/float(sweepResolution), False);



if __name__ == "__main__":
    import system_allSynapses as sys
    import info as nf
    import pylab as pl

    networkSize=3;
    i = nf.info();
    coupling_strengths_chlorine = 0.005 * np.asarray(np.matrix('0 1 1 ; 1 0 1 ; 1 1 0 '))
    n = netw.network(networkSize, info=i, coupling_strengths_chlorine=coupling_strengths_chlorine,
                                parameterSet=fh.parameters_generic(networkSize, [0.42, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.]));
    n.CYCLES = 100 
    tor = torus_3(n, info=None)
#    tor.sweepCPUInPython(3)
    tor.sweepGPUInC(70);
    pl.show()

