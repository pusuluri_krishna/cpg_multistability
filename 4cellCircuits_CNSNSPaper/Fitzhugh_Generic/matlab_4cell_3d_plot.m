%TO RUN THE SCRIPT, ENTER THE FOLLOWING COMMAND
% matlab -nodisplay -nosplash -nodesktop -r "run('./matlab_4cell_3d_plot.m');exit;"


clf('reset')
dir_path=fullfile(pwd,'image');

lw=3;

cdata_1 = imread(fullfile(dir_path, 'th_13_14.png')); %blue
cdata_2 = imread(fullfile(dir_path, 'th_12_14.png')); %green
cdata_3 = imread(fullfile(dir_path, 'th_12_13.png')); %red

% bottom
surface([0 1; 0 1], [0 0; 1 1], [0 0; 0 0], 'FaceColor', 'texturemap', 'CData', cdata_1 );

% back
surface([0 1; 0 1], [1 1; 1 1], [0 0; 1 1], 'FaceColor', 'texturemap', 'CData', cdata_2  );

% left
surface([0 0; 0 0], [0 1; 0 1], [0 0; 1 1], 'EdgeColor', 'w', 'LineWidth', lw,'FaceColor', 'texturemap', 'CData', cdata_3  );

%alpha 0.5


cdata_3_1 = imread(fullfile(dir_path, 'th_12_13_0-15.png'));
x_shift=0.15;
surface([x_shift x_shift; x_shift x_shift], [0 1; 0 1], [0 0; 1 1], 'EdgeColor', 'w', 'LineWidth', lw,'FaceColor', 'texturemap', 'CData', cdata_3_1  );
%line([x_shift,x_shift],[1,1],[0,1],'Color', 'r', 'LineStyle','--' )
%line([x_shift,x_shift],[0,1],[0,0],'Color', 'r', 'LineStyle','--' )

x_shift=0.3;
cdata_3_2 = imread(fullfile(dir_path, 'th_12_13_0-30.png'));
h=surface([x_shift x_shift; x_shift x_shift], [0 1; 0 1], [0 0; 1 1], 'EdgeColor', 'w', 'LineWidth', lw,'FaceColor', 'texturemap', 'CData', cdata_3_2  );
%line([x_shift,x_shift],[1,1],[0,1],'Color', 'r', 'LineStyle','--' )
%line([x_shift,x_shift],[0,1],[0,0],'Color', 'r', 'LineStyle','--' )

x_shift=0.45;
cdata_3_3 = imread(fullfile(dir_path, 'th_12_13_0-45.png'));
surface([x_shift x_shift; x_shift x_shift], [0 1; 0 1], [0 0; 1 1], 'EdgeColor', 'w', 'LineWidth', lw,'FaceColor', 'texturemap', 'CData', cdata_3_3  );
%line([x_shift,x_shift],[1,1],[0,1],'Color', 'r', 'LineStyle','--' )
%line([x_shift,x_shift],[0,1],[0,0],'Color', 'r', 'LineStyle','--' )


x_shift=0.6;
cdata_3_3 = imread(fullfile(dir_path, 'th_12_13_0-60.png'));
surface([x_shift x_shift; x_shift x_shift], [0 1; 0 1], [0 0; 1 1], 'EdgeColor', 'w', 'LineWidth', lw,'FaceColor', 'texturemap', 'CData', cdata_3_3  );


cdata_3_1 = imread(fullfile(dir_path, 'th_12_13_0-75.png'));
x_shift=0.75;
surface([x_shift x_shift; x_shift x_shift], [0 1; 0 1], [0 0; 1 1], 'EdgeColor', 'w', 'LineWidth', lw,'FaceColor', 'texturemap', 'CData', cdata_3_1  );
%line([x_shift,x_shift],[1,1],[0,1],'Color', 'r', 'LineStyle','--' )
%line([x_shift,x_shift],[0,1],[0,0],'Color', 'r', 'LineStyle','--' )

x_shift=0.9;
cdata_3_2 = imread(fullfile(dir_path, 'th_12_13_0-90.png'));
h=surface([x_shift x_shift; x_shift x_shift], [0 1; 0 1], [0 0; 1 1], 'EdgeColor', 'w', 'LineWidth', lw,'FaceColor', 'texturemap', 'CData', cdata_3_2  );
%line([x_shift,x_shift],[1,1],[0,1],'Color', 'r', 'LineStyle','--' )
%line([x_shift,x_shift],[0,1],[0,0],'Color', 'r', 'LineStyle','--' )

x_shift=1;
surface([x_shift x_shift; x_shift x_shift], [0 1; 0 1], [0 0; 1 1],'EdgeColor', 'w', 'LineWidth', lw, 'FaceColor', 'texturemap', 'CData', cdata_3  );
az = 353; %349
el = 37; %32
view(3);view(az, el);
hold on;
xs=[0 1 0.5 0.5] ; ys=[0.5 0.5 0.5 1]; zs=[0.5 0.5 0 0.5];
plot3(xs,ys,zs,'o','Color','w','MarkerSize',2,'MarkerFaceColor','w');
hold off;

fig = gcf;
fig.PaperUnits = 'inches';
fig.PaperPosition = [0 0 20 10];
set(gca(), 'LooseInset', get(gca(), 'TightInset'));

set(gca,'XTick',[]);set(gca,'YTick',[]);set(gca,'ZTick',[]);


print(fullfile(dir_path, '4cell3D.png'), '-dpng');
