#!/usr/bin/env python

import time

import Network_Generic_allSynapseTypes as netw
import sys
sys.path.insert(0, '../Tools')
import fitzhugh_allSynapseTypes as fh
import tools as tl
import window as win
from matplotlib.patches import Ellipse
import numpy as np
import pylab as pl
import matplotlib.cm as colormap
import matplotlib.animation as animation

class traces(win.window):
    title = 'Voltage Traces'
    figsize = (13, 2)

    def __init__(self, network, info=None, position=None):
        win.window.__init__(self, position)
        self.network = network
        self.info = info
        self.setupFigure()
        # self.fig.tight_layout()
        self.fig.canvas.mpl_connect('axes_enter_event', self.focusIn)

        #self.computeTraces()

    def setupFigure(self):
        self.fig.clear()
        self.ax = self.fig.add_subplot(111, frameon=False, yticks=[])
        self.li=[]
        for i in range(self.network.networkSize):
            zd = self.ax.plot([],[],color = colormap.gist_rainbow(i/float(self.network.networkSize)),lw=2.)
            self.li.append(zd);
        self.ax.set_xlabel(r'time (sec.)', fontsize=20)
        self.ax.set_xticklabels(np.arange(0., 1., 0.1), fontsize=15)
        self.ax.set_yticklabels(np.arange(0., 1., 0.1), fontsize=15)
        self.ax.set_xlim(0., 100.)
        self.ax.set_ylim(-20, 1.5)

    def computeTraces(self, initial_condition=None, plotit=True, printLast20=False, plotAnimation=False, noOfCyclesToAnimate=20, plotTransient=True, plotOrbit=True, noOfCyclesInOrbit=1):

        if initial_condition is None:
            #initial_condition = self.system.load_initial_condition(pl.rand(), pl.rand())
            initial_condition = self.network.load_initial_condition_generic(self.network.networkSize)
        startTime = time.time();
        #no.of cycles based on the last neuron's orbit since that's set last during system.load_initial_condition_generic() above
        Vx_i_new,d = fh.integrate_n_rk4_phasedifferences_electricExcitatoryAndInhibitoryConnections(self.network.networkSize,
            initial_condition,
            self.network.coupling_strengths_chlorine,
            self.network.coupling_strengths_potassium,
            self.network.coupling_strengths_sodium,
            self.network.coupling_strengths_electric,
            self.network.systemList[0].dt / float(self.network.systemList[0].stride),
            self.network.systemList[0].N_output(self.network.CYCLES),
            self.network.systemList[0].stride, self.network.parameterSet);
        V_i_new = Vx_i_new[:,::2]
        t = self.network.systemList[0].dt * np.arange(V_i_new.shape[0])
        endTime = time.time();
        print("time taken in seconds to compute traces :", endTime - startTime);
        #print "Fixed point phase relative to cell1: ", fh.getAttractorFixedPhaseDiff(d, len(d), networkSize)
        numCyclesToShow=20
        if plotit:
            ticks = np.asarray(t[::t.size / 10], dtype=int)

            xscale, yscale = t[-1], 2.
            for i in range(self.network.networkSize):
                if(printLast20==False):
                    tj, Vj = tl.adjustForPlotting(t, V_i_new[:, i], ratio=xscale / yscale, threshold=0.05 * xscale)
                else :
                    #plotting only the last 20 cycles
                    tj, Vj = tl.adjustForPlotting(t[-V_i_new.shape[0]*numCyclesToShow/self.network.CYCLES:], V_i_new[-V_i_new.shape[0]*numCyclesToShow/self.network.CYCLES:, i], ratio=xscale / yscale, threshold=0.05 * xscale)

                #self.ax.plot(tj, Vj - i*2, color = colormap.Spectral(i/float(self.network.networkSize)),lw=2.)
                zd = self.li[i][0];
                #print zd;
                zd.set_data(tj, Vj - i*2)


            self.ax.set_xticks(ticks)
            self.ax.set_xticklabels(ticks)

            if(printLast20==False):
                self.ax.set_xlim(t[0], t[-1])
            else :
                #plotting only the last 20 cycles
                self.ax.set_xlim(t[-V_i_new.shape[0]*numCyclesToShow/self.network.CYCLES], t[-1])

            self.fig.canvas.draw()

        if plotAnimation:
            sys = self.network.systemList[-1]
            ax = sys.ax     #only the last system is plotted due to overlaps, so we will plot the animation on top of this system; we will use the last neuron's trajectory to compute the network orbit/period
            x_raw, y = Vx_i_new[:,-2],Vx_i_new[:,-1]
            x_m, y_m = tl.splineLS1D(), tl.splineLS1D()

            ni = None
            try:
                ni = np.asarray(tl.crossings(x_raw, fh.THRESHOLD), dtype=int)  # convert to millivolts
                x, y = x_raw[ni[-noOfCyclesInOrbit-1]:ni[-1]], y[ni[-noOfCyclesInOrbit-1]:ni[-1]]
                t = tl.PI2 * np.arange(x.size) / float(x.size - 1)
                # compute smoothened curves of V,x for the values between the last two crossings
                x_m.makeModel(x, t);
                y_m.makeModel(y, t)

            except:
                print '# single_orbit:  No closed orbit found!'
                raise ValueError

            T = self.network.systemList[-1].dt * x.size  # in msec.

            phi = np.arange(500) / float(499.)

            xscale, yscale = 1., 3.
            y, x = tl.adjustForPlotting(y_m(tl.PI2 * phi), x_m(tl.PI2 * phi), ratio=xscale / yscale,
                                        threshold=0.03 * xscale)
            y[-1], x[-1] = y[0], x[0]

            sys.li_traj.set_alpha(.3)
            sys.li_ncl_x.set_alpha(.3)
            sys.li_ncl_y.set_alpha(.3)

            if plotOrbit:
                ax.plot(y, x, 'b-', lw=1., label='Network Oscillation Cycle')

            if plotTransient:
                ax.plot(Vx_i_new[:, (self.network.networkSize-1)*fh.N_EQ1+1], Vx_i_new[:, (self.network.networkSize-1)*fh.N_EQ1], 'b-', lw=0.5, alpha=0.3)

            cellsList = []
            cellsText = []
            for j in range(self.network.networkSize):
                cellsList.append(Ellipse(np.array([0,0]), 0.05, 0.1, fc=colormap.gist_rainbow(j/float(self.network.networkSize)), alpha=1))
                #cellsList.append(Ellipse(np.array([0,0]), 0.05, 0.1, fc='k'))
                cellsText.append(ax.text(0, 0, j+1, fontsize=10, color='k', ha='center', va='center', zorder=2, style='italic', weight='heavy'))
                ax.add_patch(cellsList[j])

            def update(i):
                for j in range(self.network.networkSize):
                    if plotTransient:
                        cellsList[j].center = Vx_i_new[i, j * fh.N_EQ1 + 1], Vx_i_new[i, j * fh.N_EQ1]
                        cellsText[j].set_position((Vx_i_new[i, j * fh.N_EQ1 + 1], Vx_i_new[i, j * fh.N_EQ1]))
                    else:
                        cellsList[j].center = Vx_i_new[ni[-3]+i%(ni[-1] - ni[-3]), j*fh.N_EQ1+1], Vx_i_new[ni[-3]+i%(ni[-1] - ni[-3]), j*fh.N_EQ1]
                        cellsText[j].set_position((Vx_i_new[ni[-3]+i%(ni[-1] - ni[-3]), j*fh.N_EQ1+1], Vx_i_new[ni[-3]+i%(ni[-1] - ni[-3]), j*fh.N_EQ1]))
                #sys.fig.canvas.draw()
                return cellsList

            animationInterval=20
            ani = animation.FuncAnimation(sys.fig, update, frames=(ni[-1] - ni[-2])*noOfCyclesToAnimate, interval=animationInterval, blit=False)
            pl.show()

        return t, V_i_new, d


if __name__ == "__main__":
    import system_allSynapses as sys
    networkSize = 4
    cs = 0.055 * np.asarray(np.matrix("0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0"))
    #parameterSet = fh.parameters_generic(3, parameter_input_array=[0.5, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.])

    #first 3 cells in the travelling wave regime, fourth has longer cycles
    parameterSet = np.array([0.45, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                             0.45, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                             0.45, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                             0.45, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.])

    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=cs)
    tra = traces(network=n)
    n.CYCLES = 50
    tra.computeTraces(n.load_initial_condition_generic([0.1, 0.5, 0.76, 0.]), printLast20=True, plotAnimation=True)
#   tra.computeTraces(n.load_initial_condition_generic([ 0, 0.5198556 ,  0.5198556 ,  1. , 0.5198556, 0.5198556]) )
    pl.show()

