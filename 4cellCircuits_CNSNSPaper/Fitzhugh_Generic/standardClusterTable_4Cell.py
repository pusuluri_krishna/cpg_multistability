PH = '3 Paired halfcenters'; PM = '4 Pacemakers'; AP = '4 Asymmetric Pacemakers';  S = '1 Synchronized state'; FT = '6 Full travelling waves'; MT = '12 Mixed travelling waves'; Tr = 'Transitions'; NC = 'Non-convergence' #Default is T which has no entries in the table
#colorPH = 'saddlebrown'; colorPM = 'blue'; colorAP = 'aqua'; colorS = 'green'; colorFT = 'purple'; colorMT = 'deeppink'; colorTr = 'grey'; colorNC = 'black'
colorPH = 'darkorange'; colorPM = 'blue'; colorAP = 'aqua'; colorS = 'green'; colorFT = 'purple'; colorMT = 'deeppink'; colorTr = 'grey'; colorNC = 'k'

#ClusterMean
standardClusterTable = [
            [0., 0., 0.5],
            [0., 0.5, 0.],
            [0.5, 0., 0.],
            [0.5, 0.5, 0.5],

            [0., 0., 0.4],
            [0., 0.4, 0.],
            [0.4, 0., 0.],
            [0.6, 0.6, 0.6],

            [0., 0.5, 0.5],
            [0.5, 0., 0.5],
            [0.5, 0.5, 0.],

            [0.25, 0.5, 0.75],
            [0.25, 0.75, 0.5],
            [0.5, 0.25, 0.75],
            [0.5, 0.75, 0.25],
            [0.75, 0.25, 0.5],
            [0.75, 0.5, 0.25],

            [0., 0.34, 0.67],
            [0., 0.67, 0.34],
            [0.34, 0., 0.67],
            [0.67, 0., 0.34],
            [0.34, 0.67, 0.],
            [0.67, 0.34, 0.],
            [0.34, 0.34, 0.67],
            [0.67, 0.67, 0.34],
            [0.34, 0.67, 0.34],
            [0.67, 0.34, 0.67],
            [0.67, 0.34, 0.34],
            [0.34, 0.67, 0.67],

            [0., 0., 0.],

]

#Other cluster properties corresponding to the means above :  ClusterType, Identifier of particular ClusterType, color
#The last two entries correspond to the properties of transitions (matching none of the above clusters) or non-convergence
standardClusterProperties = [
            [PM, 0, colorPM],
            [PM, 1, colorPM],
            [PM, 2, colorPM],
            [PM, 3, colorPM],

            [AP, 0, colorPM], #[AP,	0, a],
            [AP, 1, colorPM], #[AP,	1, a],
            [AP, 2, colorPM], #[AP,	2, a],
            [AP, 3, colorPM], #[AP,	3, a],

            [PH, 0, colorPH],
            [PH, 1, colorPH],
            [PH, 2, colorPH],

            [FT, 0, colorFT],
            [FT, 1, colorFT],
            [FT, 2, colorFT],
            [FT, 3, colorFT],
            [FT, 4, colorFT],
            [FT, 5, colorFT],

            [MT, 0, colorMT],
            [MT, 1, colorMT],
            [MT, 2, colorMT],
            [MT, 3, colorMT],
            [MT, 4, colorMT],
            [MT, 5, colorMT],
            [MT, 6, colorMT],
            [MT, 7, colorMT],
            [MT, 8, colorMT],
            [MT, 9, colorMT],
            [MT, 10, colorMT],
            [MT, 11, colorMT],

            [S, 0, colorS],

            [Tr, 0, colorTr],

            [NC, 0, colorNC]
]

#print standardClusterTable.__len__()
