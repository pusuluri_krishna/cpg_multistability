import traces_allSynapses as tr
import Network_Generic_allSynapseTypes as netw
import numpy as np
import system_allSynapses as sys
import pylab as pl
import fitzhugh_allSynapseTypes as fh

networkSize = 4
cs = .025 * np.asarray(np.matrix("0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0"))
#cse = 0.025 * np.asarray(np.matrix('0 0 1 0; 0 0 0 1; 1 0 0 0; 0 1 0 0'))

#parameters : I_0=0.5, epsilon_0=0.3, x_0=0., k_0=10. , E_Cl=-1.5, E_K=-1.2, E_Na=1.5, m_0=1.
#parameterSet = np.array([0.505, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.,
                         #0.505, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.,
                         #0.505, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.,
                         #0.505, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])

#cs = 0.025 * np.asarray(np.matrix("0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0"))
#parameterSet = fh.parameters_generic(networkSize, [0.435, 0.50, 0., 10., -1.5, -1.2, 1.5, 1.])
#eps 0.50 single orbit.. 0.48 twin orbit.. 0.39 twin orbit.. 0.38 single orbit as we go from 0.38 to 0.39 the transient seems to be going through a period doubling but the final orbit remains either single or double loop.. so there seems to be period doubling of the orbit but they are unstable; At 0.33 it gvs rise to 4 pacemakers, which then combine to give rise to 3 PHs at 0.3

#cs = 0.025 * np.asarray(np.matrix("0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0"))
#parameterSet = fh.parameters_generic(networkSize, [0.4, 0.433884434968, 0., 10., -1.5, -1.2, 1.5, 1.]) ###torus formation for the synchronous state : period doubling bifurcation at eps = 0.433884434968 (laptop), below this eps there are two orbits that alternate , above ...69 or 70.. there is only one orbit.. the opposite bifurcation happens at eps 0.3 .. above that there is the double orbit, below that they merge.. 0.31 twin orbit, 0.32 4-orbit and so on.. Here these period doubling orbits are stable
#initialCondition = [0.04,0.03,0.02,0.01]

#parameterSet = fh.parameters_generic(networkSize, [0.4, 0.34, 0., 10., -1.5, -1.2, 1.5, 1.])

parameterSet = fh.parameters_generic(networkSize, [0.435, 0.48, 0., 10., -1.5, -1.2, 1.5, 1.])

n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=cs)#, coupling_strengths_electric=cse)
tra = tr.traces(network=n)
n.CYCLES = 10

#initialCondition = [0.,0.25,0.5,.75]
#initialCondition = [0.,0.03,0.5,.47]
#initialCondition = [0.,0.,0.33,.66]
#initialCondition = [0.0,0.05,0.1,0.1]
initialCondition = [0.04,0.03,0.02,0.01]

tra.computeTraces(n.load_initial_condition_generic(initialCondition), printLast20=False, plotAnimation=True, noOfCyclesToAnimate=10, plotTransient=True, plotOrbit=True, noOfCyclesInOrbit=2)
pl.show()
