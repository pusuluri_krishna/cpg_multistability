#use 'Agg' backend to run from command line and write to a file
import matplotlib
matplotlib.use('Agg')
import sys
sys.path.insert(0, '../Tools')
import info as nf
import pylab as pl
import torus_3_allSynapses as torus
import Network_Generic_allSynapseTypes as netw
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

networkSize=3;
i = nf.info();
#parameters : I_0=0.5, epsilon_0=0.3, x_0=0., k_0=10. , E_Cl=-1.5, E_K=-1.2, E_Na=1.5, m_0=1.
parameterSet = np.array([0.41, 0.1, 0., 10., -1.5, -1.2, 1.5, 1.,
                         0.41, 0.1, 0., 10., -1.5, -1.2, 1.5, 1.,
                         0.59, 0.1, 0., 10., -1.5, -1.2, 1.5, 1.])
fileName = 'Output/torus3CellPacemakers_Currents1.pdf'
pp = PdfPages(fileName)

def plotTorus(coupling_strength_chlorine = 0.005, sweepSize=70,useGPU=False):
    coupling_strengths_chlorine = coupling_strength_chlorine * np.asarray(np.matrix('0 1 1 ; 1 0 1 ; 1 1 0 '))
    n = netw.network(networkSize, info=i, coupling_strengths_chlorine=coupling_strengths_chlorine,
                                parameterSet=parameterSet);
    tor = torus.torus_3(n, info=None)
    if(useGPU):
        tor.sweepGPUInC(sweepSize);
    else:
        tor.sweepCPUInPython(sweepSize)
    #pl.show()
    tor.fig.suptitle("Symmetric network - I1=I2=0.41, I3=0.59, Coupling strengths = %s,"%(coupling_strength_chlorine))
    pp.savefig()

plotTorus(0.0)
plotTorus(0.005)
plotTorus(0.010)
plotTorus(0.020)
plotTorus(0.030)
plotTorus(0.040)
plotTorus(0.050)
plotTorus(0.060)
plotTorus(0.070)
pp.close()
