#All label positions adjusted for 'Smaug' workstation at lab

#from __future__ import division
import matplotlib
matplotlib.use('Agg')
import ctypes as ct
from pylab import *
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid.inset_locator import inset_axes
from mpl_toolkits.axes_grid.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid.inset_locator import mark_inset
import matplotlib.patches as patches
import pickle
import gzip
from scipy.integrate import ode
from matplotlib import transforms
import matplotlib.image as mpimg
import sweepClusters
import pickle
import gzip
import Network_Generic_allSynapseTypes as netw
import numpy as np
import fitzhugh_allSynapseTypes as fh
import info as nf
import tools as tl
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import standardClusterTable_4Cell as sct
#from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.patches import FancyArrowPatch,Circle,Patch
import matplotlib.cm as colormap
import time
from matplotlib.patches import Ellipse
import matplotlib.animation as animation

N = 30000 ; dataSaved = True; sweepSize = 2000

paperWidthInInches = 7.25

matplotlib.rcParams['savefig.dpi'] = 600 #2*sweepSize/figSize #300
defaultFontSize = 8; largeFontSize = 10; indexFontSize = 12;
matplotlib.rcParams.update({'font.size': largeFontSize,
              'axes.labelsize': largeFontSize,
              'axes.titlesize': largeFontSize,
              'font.size': defaultFontSize,
              'legend.fontsize': largeFontSize,
              'xtick.labelsize': defaultFontSize,
              'ytick.labelsize': defaultFontSize})

#comment these two for rainbow text function to run properly, along with setting fig.dpi = 1200 described near rainbow text
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=0.)
arrowprops = dict(arrowstyle="-", connectionstyle="arc3", facecolor='white', lw=1.,
                  ec="white")  # ,shrink=0.05, width=defaultFontSize/10.)
bbox_props_fig = dict(boxstyle="square,pad=0.001", fc="white", ec="k", lw=0)
bbox_props_index = dict(boxstyle="square,pad=0.", fc="white", ec="k", lw=0.)
bbox_props_label = dict(boxstyle="square,pad=0.01", fc="white", ec="k", lw=0	)

networkSize = 4;

ImagesToPlot = {0, 1, 2, 3, 4, 5, 6}
ImagesToPlot = {0}

def drawNetwork(ax, couplingStrengths, networkSize=4, plotAxis=False, stretchFactor=1., labelSize = largeFontSize, isColored=False):
    network_origin = np.array([0.5, 0.5])
    cell_radius = stretchFactor*0.25 / networkSize
    secondary_cell_radius = .225 * cell_radius

    # ax = fig.add_axes([-0.12, -0.1, 1.1, 1.33])

    def getCellPosition(cellIndex):
        # for a cell index between 0 and networkSize-1, returns its coordinates
        return network_origin + cell_radius * (networkSize) * np.array(
            [math.cos(3 * math.pi / 4 - math.pi * 2 * cellIndex / networkSize),
             math.sin(3 * math.pi / 4 - math.pi * 2 * cellIndex / networkSize)])

    for i in range(0, networkSize):
        for j in range(0, networkSize):

            startCoordinates = getCellPosition(i)
            endCoordinates = getCellPosition(j)
            differenceVector = (endCoordinates - startCoordinates)
            differenceVectorLength = math.sqrt(differenceVector[0] ** 2 + differenceVector[1] ** 2)
            perpendicularVector = np.array([-differenceVector[1], differenceVector[0]])
            perpendicularVectorLength = math.sqrt(perpendicularVector[0] ** 2 + perpendicularVector[1] ** 2)

            if (couplingStrengths[i, j] != 0):
                shiftdifferenceVector = differenceVector * (
                cell_radius + secondary_cell_radius) / differenceVectorLength
                shiftPerpendicularVector = perpendicularVector * secondary_cell_radius / perpendicularVectorLength
                if (couplingStrengths[j, i] == 0):
                    shiftPerpendicularVector = 0
                ax.add_patch(FancyArrowPatch(startCoordinates + shiftPerpendicularVector,
                                             endCoordinates + shiftPerpendicularVector,
                                             arrowstyle=u'-', lw=2, mutation_scale=cell_radius * 300,
                                             # shrinkA=400*cell_radius,shrinkB=400*cell_radius,
                                             color='k'))
                ax.add_patch(
                    Circle(endCoordinates - shiftdifferenceVector * 0.95 + shiftPerpendicularVector , secondary_cell_radius,
                           fc='k'))  # * =0.95 since the shift computation is done for the arrow at the center (when the two circles will be tangential whereas in reality they're slightly apart)

                #                    ax.text((startCoordinates[0]+endCoordinates[0])/2 - cell_radius + shiftPerpendicularVector[0],
                #                            (startCoordinates[1]+endCoordinates[1])/2 + cell_radius/3 + shiftPerpendicularVector[1],
                #                            '%.4f\n'%(coupling_strengths[i,j]), fontsize=200*cell_radius)

    for i in range(0, networkSize):
        position = np.array(getCellPosition(i))
        color= colormap.gist_rainbow((networkSize-i-1) / float(networkSize)) if isColored else 'k'
        ax.add_patch(
            Circle(position, cell_radius, fc=color))
        # http://wiki.scipy.org/Cookbook/Matplotlib/Show_colormaps
        if isColored:
            ax.text(position[0], position[1], r'$\boldsymbol{' + str(i + 1) + '}$', fontsize=labelSize, color='k',
                    ha='center', va='center')
        else:
            ax.text(position[0], position[1], r'$\boldsymbol{' + str(i + 1) + '}$', fontsize=labelSize, color='w',
                    ha='center', va='center')

    if not plotAxis: ax.set_axis_off()
    return

def computeTracesValues(network, initial_condition=None):
    if initial_condition is None:
        #initial_condition = self.system.load_initial_condition(pl.rand(), pl.rand())
        initial_condition = network.load_initial_condition_generic(network.networkSize)
    startTime = time.time();
    #no.of cycles based on the last neuron's orbit since that's set last during system.load_initial_condition_generic() above
    Vx_i_new,d = fh.integrate_n_rk4_phasedifferences_electricExcitatoryAndInhibitoryConnections(network.networkSize,
initial_condition,network.coupling_strengths_chlorine, network.coupling_strengths_potassium, network.coupling_strengths_sodium, network.coupling_strengths_electric, network.systemList[0].dt / float(network.systemList[0].stride), network.systemList[0].N_output(network.CYCLES), network.systemList[0].stride, network.parameterSet);

    endTime = time.time();
    print("time taken in seconds to compute traces :", endTime - startTime);
    #print "Fixed point phase relative to cell1: ", fh.getAttractorFixedPhaseDiff(d, len(d), networkSize)

    return Vx_i_new, d


# If numCyclesToShow >0, then the value given in the parameter is used to show those number of traces,
# otherwise all traces are shown
def plotTraces(ax, network, t, V_i_new, d=None, numCyclesToShow=20, isMultiColored=True, monoColor='darkgray', showTickLabels=False, showBoxes=True, printFirstCycles=False):
    ax.set_ylim(bottom=-2*network.networkSize, top=2.0)
    printLastTraces= True if numCyclesToShow>0 else False;
    ticks = np.asarray(t[::t.size / 10], dtype=int)
    li = []; color=[]; cell1Crossings=[]
    ax.set_yticks([]);ax.set_yticklabels([])
    ax.set_xticks([]);ax.set_xticklabels([])
    ax.tick_params(axis='x', direction='inout', pad=0, length=0, width=1, colors='k')

    if showTickLabels:
        ax2 = ax.twiny(); #Duplicate axes to provide ticks and label on the top
        ax2.tick_params(axis='x', direction='inout', pad=0, length=0, width=1, colors='k')

    for i in range(network.networkSize):
        color.append(colormap.gist_rainbow((network.networkSize-i-1) / float(network.networkSize)) if isMultiColored else monoColor)
        zd = ax.plot([], [], color=color[i], lw=2.)
        li.append(zd);

    cyclesIndexLength= int(V_i_new.shape[0] * numCyclesToShow / network.CYCLES)
    xscale, yscale = t[-1], 2.
    for i in range(network.networkSize):
        if(printLastTraces==False):
            tj, Vj = tl.adjustForPlotting(t, V_i_new[:, i], ratio=xscale / yscale, threshold=0.05 * xscale)
        else :
            if printFirstCycles :
                #plotting only the last cycles
                tj, Vj = tl.adjustForPlotting(t[:cyclesIndexLength], V_i_new[:cyclesIndexLength, i], ratio=xscale / yscale, threshold=0.05 * xscale)
            else:
                #plotting only the last cycles
                tj, Vj = tl.adjustForPlotting(t[-cyclesIndexLength:], V_i_new[-cyclesIndexLength:, i], ratio=xscale / yscale, threshold=0.05 * xscale)

        zd = li[i][0];
        zd.set_data(tj, Vj - i*2)
        ni_up = np.asarray(tl.crossings(Vj , fh.THRESHOLD), dtype=int)  # convert to millivolts
        ni_down = np.asarray(tl.crossings(-Vj , -fh.THRESHOLD), dtype=int)  # convert to millivolts
        traceheight = 1.8; alphaTransparency = 0.4
        tracewidth=tj[ni_down[0]]-tj[ni_up[0]] if ni_down[0] > ni_up[0] else tj[ni_down[1]]-tj[ni_up[0]]
        if showBoxes:
            ax.add_patch(patches.Rectangle((tj[ni_up[0]]-tracewidth*0.25, -0.8-i*2),tracewidth*1.5 , traceheight,
                                           fc=color[i], alpha=alphaTransparency))
            ax.add_patch(patches.Rectangle((tj[ni_up[1]]-tracewidth*0.25, -0.8-i*2),tracewidth*1.5 , traceheight,
                                           fc=color[i], alpha=alphaTransparency))
            if numCyclesToShow > 10:
                #Show last two cycles as well
                ax.add_patch(patches.Rectangle((tj[ni_up[-2]]-tracewidth*0.25, -0.8-i*2),tracewidth*1.5 , traceheight,
                                               fc=color[i], alpha=alphaTransparency))
                ax.add_patch(patches.Rectangle((tj[ni_up[-1]]-tracewidth*0.25, -0.8-i*2),tracewidth*1.5 , traceheight,
                                               fc=color[i], alpha=alphaTransparency))
        if i==0:
            cell1Crossings=ni_up
            for x in ni_up:
                ax.axvline(x=tj[x], linewidth=1., color='k', linestyle=':')
            ax.set_xticks(tj[ni_up]); #ax.set_xticklabels([]);
            if showTickLabels:
                ax2.xaxis.set_tick_params(labeltop='on'); ax2.set_xticks(tj[ni_up]); #ax2.set_xticklabels([])

    if showTickLabels and d.all!=None:
        dj= d if(printLastTraces) else d[-len(cell1Crossings):, :]
        dj = dj.round(decimals=2)
        ax.xaxis.set_tick_params(pad=2., labeltop='off', labelbottom='on'); ax.set_xticklabels(dj[:,1])
        ax2.xaxis.set_tick_params(pad=0.5, labeltop='on', labelbottom='off'); ax2.set_xticklabels(dj[:,0]);

    if(printLastTraces==False):
        ax.set_xlim(t[0], t[-1])
    else :
        ax.set_xlim(t[0], t[cyclesIndexLength]) if printFirstCycles else ax.set_xlim(t[-cyclesIndexLength], t[-1])
    if showTickLabels: ax2.set_xlim(ax.get_xlim())
    #fig.canvas.draw()
    return

def plotSystem(ax, network, Vx_i_new, saveAnimation=False, indexToPlot=0, plotPast=False, pastPercent=0.04, pastNumber=10, noOfCyclesToAnimate=20, plotTransient=True, plotOrbit=True, orbitColor='b', noOfCyclesInOrbit=1,showColoredCells=True, showCellLabels=False):
    sys = network.systemList[-1]

    ax.plot(sys.li_ncl_x.get_data()[0], sys.li_ncl_x.get_data()[1], linestyle='-', color='dimgray', lw=1., alpha=0.3, zorder=-2)
    ax.plot(sys.li_ncl_y.get_data()[0], sys.li_ncl_y.get_data()[1], linestyle='-', color='dimgray', lw=1., alpha=0.3, zorder=-2)
    ax.plot(sys.li_traj.get_data()[0], sys.li_traj.get_data()[1], 'k-', lw=2., alpha=0.3, zorder=-2)
    ax.axhline( linewidth=1, color='k', linestyle='dotted', lw=1., alpha=0.3, zorder=-2)

    x_raw, y = Vx_i_new[:, -2], Vx_i_new[:, -1]
    x_m, y_m = tl.splineLS1D(), tl.splineLS1D()

    ni = None
    try:
        ni = np.asarray(tl.crossings(x_raw, fh.THRESHOLD), dtype=int)  # convert to millivolts
        x, y = x_raw[ni[-noOfCyclesInOrbit - 1]:ni[-1]], y[ni[-noOfCyclesInOrbit - 1]:ni[-1]]
        t = tl.PI2 * np.arange(x.size) / float(x.size - 1)
        # compute smoothened curves of V,x for the values between the last two crossings
        x_m.makeModel(x, t);
        y_m.makeModel(y, t)

    except:
        print '# single_orbit:  No closed orbit found!'
        raise ValueError

    T = network.systemList[-1].dt * x.size  # in msec.

    phi = np.arange(500) / float(499.)

    xscale, yscale = 1., 3.
    y, x = tl.adjustForPlotting(y_m(tl.PI2 * phi), x_m(tl.PI2 * phi), ratio=xscale / yscale,
                                threshold=0.03 * xscale)
    y[-1], x[-1] = y[0], x[0]

    sys.li_traj.set_alpha(.3)
    sys.li_ncl_x.set_alpha(.3)
    sys.li_ncl_y.set_alpha(.3)

    if plotOrbit:
        ax.plot(y, x, color=orbitColor, linestyle='-', lw=1.5, label='Network Oscillation Cycle', zorder=-2)

    """
    if plotTransient:
        ax.plot(Vx_i_new[:, (network.networkSize - 1) * fh.N_EQ1 + 1],
                Vx_i_new[:, (network.networkSize - 1) * fh.N_EQ1], 'b-', lw=0.5, alpha=0.3, zorder=-2)
    """
    color=[ colormap.gist_rainbow(i / float(network.networkSize)) if showColoredCells else 'grey'  for i in range(network.networkSize)]
    cellLabelColor= 'k' if showColoredCells else 'w'

    if plotPast:
        cellsList=[]
        for k in range(pastNumber):
            for j in range(network.networkSize):
                cell = None
                if k==0:
                    cell = Ellipse(np.array([Vx_i_new[ni[-3] + int(indexToPlot-k*(1./pastNumber)*pastPercent*(ni[-1] - ni[-3])) % (ni[-1] - ni[-3]), j * fh.N_EQ1 + 1],
                                             Vx_i_new[ ni[-3] + int(indexToPlot - k*(1./pastNumber)*pastPercent*(ni[-1] - ni[-3])) % (ni[-1] - ni[-3]), j * fh.N_EQ1]]),
                                             (1-(1./pastNumber)*k)*0.05, (1-(1./pastNumber)*k)*0.1, fc=color[j],alpha=1)
                else:
                    cell = Ellipse(np.array([Vx_i_new[ni[-3] + int(indexToPlot-k*(1./pastNumber)*pastPercent*(ni[-1] - ni[-3])) % (ni[-1] - ni[-3]), j * fh.N_EQ1 + 1],
                                             Vx_i_new[ ni[-3] + int(indexToPlot - k*(1./pastNumber)*pastPercent*(ni[-1] - ni[-3])) % (ni[-1] - ni[-3]), j * fh.N_EQ1]]),
                                             (1-(1./pastNumber)*k)*0.05*0.75, (1-(1./pastNumber)*k)*0.1*0.75, fc=color[j],alpha=1)
                ax.add_patch(cell)
        fig.canvas.draw()
        return

    cellsList = []
    cellsText = []
    for j in range(network.networkSize):
        if showColoredCells:
            cellsList.append(Ellipse(np.array([0, 0]), 0.05, 0.1, fc=color[j], alpha=1))
        else:
            cellsList.append(Ellipse(np.array([0, 0]), 0.05, 0.1, fc=color[j], ec='k', lw=1, alpha=1))

        if showCellLabels :
            cellsText.append(ax.text(0, 0, j + 1, fontsize=10, color=cellLabelColor, ha='center', va='center', zorder=2, style='italic',
                    weight='heavy', alpha=1))
        ax.add_patch(cellsList[j])

    def update(i):
        for j in range(network.networkSize):
            if plotTransient:
                cellsList[j].center = Vx_i_new[i, j * fh.N_EQ1 + 1], Vx_i_new[i, j * fh.N_EQ1]
                if showCellLabels:
                    cellsText[j].set_position((Vx_i_new[i, j * fh.N_EQ1 + 1], Vx_i_new[i, j * fh.N_EQ1]))
            else:
                cellsList[j].center = Vx_i_new[ni[-3] + i % (ni[-1] - ni[-3]), j * fh.N_EQ1 + 1], Vx_i_new[
                    ni[-3] + i % (ni[-1] - ni[-3]), j * fh.N_EQ1]
                if showCellLabels:
                    cellsText[j].set_position((Vx_i_new[ni[-3] + i % (ni[-1] - ni[-3]), j * fh.N_EQ1 + 1],
                                           Vx_i_new[ni[-3] + i % (ni[-1] - ni[-3]), j * fh.N_EQ1]))
        # sys.fig.canvas.draw()
        return cellsList

    if saveAnimation:
        #https://matplotlib.org/2.1.2/gallery/animation/basic_example_writer_sgskip.html
        animationInterval = 20
        ani = animation.FuncAnimation(sys.fig, update, frames=(ni[-1] - ni[-2]) * noOfCyclesToAnimate,
                                      interval=animationInterval, blit=False)
        return ani
    else:
        update(indexToPlot)
        fig.canvas.draw()
    return

def sweepTorus(network, sweepResolution=3):
    sweepResolutionFloat = float(sweepResolution);
    initial_condition_array=[]
    for i in range(sweepResolution):
        for j in range(sweepResolution):
                    initial_condition = network.load_initial_condition_generic([0, i/sweepResolutionFloat,
                                                                                    j/sweepResolutionFloat] )
                    initial_condition_array.extend(initial_condition)

    startTime = time.time();
    #no.of cycles is based on last neuron
    d, p, noP = fh.sweeptraces_electricExcitatoryAndInhibitoryConnections(network.networkSize,initial_condition_array,
                                                                  sweepResolution*sweepResolution,
                                                                  network.coupling_strengths_chlorine,
                                                                  network.coupling_strengths_potassium,
                                                                  network.coupling_strengths_sodium,
                                                                  network.coupling_strengths_electric,
                                                                  network.systemList[0].dt/
                                                                    float(network.systemList[0].stride),
                                                                  network.systemList[0].N_output(network.CYCLES),
                                                                  network.CYCLES,
                                                                  network.systemList[0].stride,
                                                                  network.parameterSet, withFixedPointDetection=True)

    #sweepClusters.clusterize(d);
    endTime = time.time();
    print("time taken in seconds for sweep :", endTime - startTime);
    return d, p, noP

def plotTorus(ax, d, p, noP, sweepResolution=3 ):
    sweepResolutionFloat = float(sweepResolution);
    for i in range(sweepResolution):
        for j in range(sweepResolution):
            ax.plot(i / sweepResolutionFloat, j / sweepResolutionFloat, 'o',
                    c=tl.clmap(tl.PI2 * (d[i * sweepResolution + j, 1]), tl.PI2 * ((d[i * sweepResolution + j, 0]))),ms=2.)

    fig.canvas.draw()
    return

"""
#plotTorus with imshow()
def plotTorus(ax, d, p, noP, sweepResolution=3 ):
    sweepResolutionFloat = float(sweepResolution);
    imageData = np.zeros((sweepResolution, sweepResolution, 3), dtype='float')
    for i in range(sweepResolution):
        for j in range(sweepResolution):
            #ax.plot(i / sweepResolutionFloat, j / sweepResolutionFloat, 's',c=tl.clmap(tl.PI2 * (d[i * sweepResolution + j, 1]), tl.PI2 * ((d[i * sweepResolution + j, 0]))),ms=1.2)
            clr=tl.clmap(tl.PI2 * (d[j * sweepResolution + i, 1]), tl.PI2 * ((d[j * sweepResolution + i, 0])))
            imageData[i, j, 0] = clr[0]; imageData[i, j, 1] = clr[1]; imageData[i, j, 2] = clr[2]
    ax.imshow(imageData,extent=[0,1,0,1], origin='lower', interpolation='bicubic')
    fig.canvas.draw()
    return
"""

# Simple one way closed loop
csChlorineOneWayClosedLoop = np.asarray(np.matrix('0 1 0 0; 0 0 1 0; 0 0 0 1; 1 0 0 0 '))
# Simple two way closed loop
csChlorineTwoWayClosedLoop = np.asarray(np.matrix('0 1 0 1; 1 0 1 0; 0 1 0 1; 1 0 1 0 '))
# Mixed Rearranged
csChlorineMixed_Rearranged = np.asarray(np.matrix('0 1 1 0; 0 0 1 1; 1 0 0 1; 1 1 0 0 '))
# FullyConnected
csChlorineFullyConnected = np.asarray(np.matrix('0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0 '))


####################### Cell3 image: IMAGE 0 ###############################################################################
if 0 in ImagesToPlot:
    print 'Generating Image0'

    sweepResolution = 70*2; plotDataSaved = True;


    # FullyConnected-3cell
    csChlorineFullyConnected3Cell = np.asarray(np.matrix('0 1 1 ; 1 0 1 ; 1 1 0 '))
    cs = 0.01 * csChlorineFullyConnected3Cell
    # parameterSet = fh.parameters_generic(3, parameter_input_array=[0.5, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.])

    # first 3 cells in the travelling wave regime, fourth has longer cycles
    parameterSet = np.array([0.426, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                             0.426, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                             0.426, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.])

    n = netw.network(networkSize=3, parameterSet=parameterSet, coupling_strengths_chlorine=cs)
    n.CYCLES = 100
    if not plotDataSaved:
        pickleDataFile0 = gzip.open('Image0_3cellPoincare.gz', 'wb')
    else:
        pickleDataFile0 = gzip.open('Image0_3cellPoincare.gz', 'rb')

    Vx_i_new_1, Vx_i_new_2 = None, None
    if not plotDataSaved:
        # Pace Maker
        Vx_i_new_1, d_1 = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.0, 0.1, 0.1]))
        # Travelling wave
        Vx_i_new_2, d_2 = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.0, 0.2, 0.05]))
        # Pace Maker
        Vx_i_new_3, d_3 = computeTracesValues(n, initial_condition=n.load_initial_condition_generic( [0.0, 0.9, 0.908]))
        # Travelling wave
        Vx_i_new_4, d_4 = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.0, 0.9, 0.88]))

        d, p, noP = sweepTorus(network=n, sweepResolution=sweepResolution)

        pickle.dump( [Vx_i_new_1,Vx_i_new_2,d_1, d_2, d_3, d_4, d,p, noP], pickleDataFile0)

    else:
        [Vx_i_new_1, Vx_i_new_2, d_1, d_2, d_3, d_4, d, p, noP] = pickle.load(pickleDataFile0)

    V_i_new_1 = Vx_i_new_1[:,::2]
    t_1 = n.systemList[0].dt * np.arange(V_i_new_1.shape[0])
    V_i_new_2 = Vx_i_new_2[:,::2]
    t_2 = n.systemList[0].dt * np.arange(V_i_new_2.shape[0])

    """
    figSizeX = 10; figSizeY = 8;
    rows = 16; columns = 10;
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    #######Layout 1 ##############
    gs1 = gridspec.GridSpec(rows, columns)
    gs1.update(wspace=0.5, hspace=12.)  # set the spacing between axes.

    ax1Left = plt.subplot(gs1[0:10, 0:5]) #System
    ax1Middle = plt.subplot(gs1[0:10, 5:10]) #Torus
    ax1RightTop = plt.subplot(gs1[10:16, 0:3]) #network
    ax1RightMiddle = plt.subplot(gs1[10:13, 3:]) #Traces PM
    ax1RightBottom = plt.subplot(gs1[13:16, 3:]) #Traces TW
    """

    figSizeX = paperWidthInInches; figSizeY = paperWidthInInches/2.;
    rows = 1; columns = 2;
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(rows, columns)
    gs1.update(wspace=0.15, hspace=0.)  # set the spacing between axes.
    axSystem = plt.subplot(gs1[:, 0]) #System
    axTorus = plt.subplot(gs1[:, 1]) #Torus

    # Plot System
    plotSystem(axSystem, n, Vx_i_new_2, saveAnimation=False, indexToPlot=140, plotPast=True, pastPercent=0.1, pastNumber=30, noOfCyclesToAnimate=20, plotTransient=True, plotOrbit=False, noOfCyclesInOrbit=1)
    axSystem.set_xlim(-0.05, 1.05)
    axSystem.set_ylim(-1.5, 1.5)
    axSystem.text(0.5, -0.04, r'Recovery $x$', ha='center', va='center', transform=axSystem.transAxes,
                  rotation=0, color='darkslategray', fontsize=largeFontSize, weight='roman')
    axSystem.text(1., -0.04, r'$1.0$', ha='right', va='center', transform=axSystem.transAxes,
                  fontsize=defaultFontSize, rotation=0)
    axSystem.text(0., -0.04, r'$0.0$', ha='left', va='center', transform=axSystem.transAxes,
                  fontsize=defaultFontSize, rotation=0)
    axSystem.text(-0.03, 0.52, r'Voltage $V$', ha='center', va='center', transform=axSystem.transAxes,
                  rotation=90, color='darkslategray', fontsize=largeFontSize, weight='roman')
    axSystem.text(-0.03, 0., r'$-1.5$', ha='center', va='bottom', transform=axSystem.transAxes,
                  fontsize=defaultFontSize, rotation=90)
    axSystem.text(-0.03, 1., r'$1.5$', ha='center', va='top', transform=axSystem.transAxes,
                  fontsize=defaultFontSize, rotation=90)
    axSystem.text(0.06, 0.52, r'$V_{th}=0$', ha='center', va='center', transform=axSystem.transAxes,
                  fontsize=defaultFontSize)

    axSystem.annotate(r'$\frac{dV}{dt}=0$', xy=(0, 1.2), xytext=(0, 1.2), bbox=bbox_props, color='k', rotation=-8, fontsize=largeFontSize)
    axSystem.annotate(r'$\frac{dx}{dt}=0$', xy=(0.93, 1.15), xytext=(0.93, 1.15), bbox=bbox_props, color='k', rotation=90, fontsize=largeFontSize)


    #plotAnimation=False, noOfCyclesToAnimate=20, plotTransient=True, plotOrbit=True, noOfCyclesInOrbit=1)

    # Plot Torus
    plotTorus(axTorus, d, p, noP, sweepResolution=sweepResolution)

    #Draw traces on torus
    #tl.plot_phase_2D(d_1[:, 0], d_1[:, 1], axes=ax1Middle, color='w', PI=0.5, arrows=True)
    #tl.plot_phase_2D(d_2[:, 0], d_2[:, 1], axes=ax1Middle, color='w', PI=0.5, arrows=True)
    #tl.plot_phase_2D(d_3[:, 0], d_3[:, 1], axes=ax1Middle, color='w', PI=0.5, arrows=True)
    #tl.plot_phase_2D(d_4[:, 0], d_4[:, 1], axes=ax1Middle, color='w', PI=0.5, arrows=True)

    axTorus.plot(0.007, 0.5, 'o', color='white', ms=5)
    axTorus.plot(.993, 0.5, 'o', color='white', ms=5)
    axTorus.plot(0.5, 0.007, 'o', color='white', ms=5)
    axTorus.plot(0.5, 0.993, 'o', color='white', ms=5)
    axTorus.plot(0.54, 0.54, 'o', color='white', ms=5)
    axTorus.plot(0.33, 0.66, 'o', color='white', ms=5)
    axTorus.plot(0.66, 0.33, 'o', color='white', ms=5)

    axTorus.text(0.5, -0.05, "${\Delta\\theta_{12}}$", ha='center', va='center', transform=axTorus.transAxes,
                 rotation=0, color='darkslategray', fontsize=largeFontSize, weight='roman')
    axTorus.text(0., -0.04, "$0.0$", ha='left', va='center', transform=axTorus.transAxes,
                 fontsize=defaultFontSize, rotation=0)
    axTorus.text(1.0, -0.04, "$1.0$", ha='right', va='center', transform=axTorus.transAxes,
                 fontsize=defaultFontSize, rotation=0)
    axTorus.text(-0.025, 0.52, "$\Delta\\theta_{13}$", ha='center', va='center', transform=axTorus.transAxes,
                 rotation=90, color='darkslategray', fontsize=largeFontSize, weight='roman')
    axTorus.text(-0.02, 0., "$0.0$", ha='center', va='bottom', transform=axTorus.transAxes,
                 fontsize=defaultFontSize, rotation=90)
    axTorus.text(-0.02, 1.0, "$1.0$", ha='center', va='top', transform=axTorus.transAxes,
                 fontsize=defaultFontSize, rotation=90)

    axTorus.set_xlim(0, 1.); axTorus.set_ylim(0., 1.)

    axSystem.set_xticks([]); axSystem.set_yticks([]);
    axTorus.set_xticks([]); axTorus.set_yticks([])
    axSystem.text(-0.05, .75, '(b)', ha='right', va='center', transform=axSystem.transAxes, fontsize=largeFontSize)
    axTorus.text(-0.05, .75, '(e)', ha='right', va='center', transform=axTorus.transAxes, fontsize=largeFontSize)

    plt.subplots_adjust(left=0., right=1., bottom=0., top=1.)
    margins(0, 0)
    gca().xaxis.set_major_locator(NullLocator())
    gca().yaxis.set_major_locator(NullLocator())
    fig.savefig('Image0_1.png', bbox_inches='tight', pad_inches=0)

    ######################### Image 0_2 ###################
    figSizeX = paperWidthInInches; figSizeY = .3*paperWidthInInches;
    rows = 2; columns = 10;
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(rows, columns)
    gs1.update(wspace=0.5, hspace=0.5)  # set the spacing between axes.

    axNetwork = plt.subplot(gs1[:, 0:3]) #network
    axTracesPM = plt.subplot(gs1[0, 3:]) #Traces PM
    axTracesTW = plt.subplot(gs1[1, 3:]) #Traces TW

    # Plot Network
    drawNetwork(axNetwork, couplingStrengths=csChlorineFullyConnected3Cell, networkSize=n.networkSize, plotAxis=False, isColored=True, labelSize=largeFontSize, stretchFactor=0.8)
    xlim = [0.23, 0.83];
    ylim = [0.165, 0.78]
    axNetwork.set_xlim(xlim); axNetwork.set_ylim(ylim)

    # Plot Traces
    plotTraces(axTracesPM, n, t_1, V_i_new_1, d= d_1, numCyclesToShow=11, showTickLabels=True, printFirstCycles=True)
    plotTraces(axTracesPM, n, t_1, V_i_new_1, d= d_1, numCyclesToShow=11, showTickLabels=True, printFirstCycles=True)
    plotTraces(axTracesTW, n, t_2, V_i_new_2, d= d_2, numCyclesToShow=11, showTickLabels=True, printFirstCycles=True)
    plotTraces(axTracesTW, n, t_2, V_i_new_2, d=d_2, numCyclesToShow=11, showTickLabels=True, printFirstCycles=True)
    #### plotTraces somehow top labels are not shown for the last plotTraces, hence doing it twice for the last one!!! Since this would alter transperancy of the boxes in the last one, doing the other traces also twice!!

    axTracesPM.text(-0.03, 1.05, "${\Delta\\theta_{12}}$", ha='right', va='bottom', transform=axTracesPM.transAxes,
                    rotation=0, color='w', fontsize=largeFontSize, weight='roman', bbox=bbox_props_fig)
                        #above text is dummy in white, to increase fig size
    axTracesPM.text(-0.03, 0.99, "${\Delta\\theta_{12}}$", ha='right', va='bottom', transform=axTracesPM.transAxes,
                    rotation=0, color='darkslategray', fontsize=largeFontSize, weight='roman', bbox=bbox_props_fig)
    axTracesPM.text(-0.03, 0., "$\Delta\\theta_{13}$", ha='right', va='top', transform=axTracesPM.transAxes,
                    rotation=0, color='darkslategray', fontsize=largeFontSize, weight='roman')

    axTracesTW.text(-0.03, 0.99, "${\Delta\\theta_{12}}$", ha='right', va='bottom', transform=axTracesTW.transAxes,
                    rotation=0, color='darkslategray', fontsize=largeFontSize, weight='roman', bbox=bbox_props_fig)
    axTracesTW.text(-0.03, 0., "$\Delta\\theta_{13}$", ha='right', va='top', transform=axTracesTW.transAxes,
                    rotation=0, color='darkslategray', fontsize=largeFontSize, weight='roman')

    axTracesTW.add_patch(FancyArrowPatch((265, 1), (293, -5.75), lw=0.1, color='k', ls='-', mutation_scale=10, zorder=4))
    axTracesTW.add_patch(FancyArrowPatch((300, 1), (328, -5.75), lw=0.1, color='k', ls='-', mutation_scale=10, zorder=4))

    axNetwork.set_xticks([]); axNetwork.set_yticks([]);
    axNetwork.text(0, 0.5, '(a)', ha='right', va='top', transform=axNetwork.transAxes, fontsize=largeFontSize)
    axTracesPM.text(-0.05, 0.5, '(c)', ha='right', va='top', transform=axTracesPM.transAxes, fontsize=largeFontSize)
    axTracesTW.text(-0.05, 0.5, '(d)', ha='right', va='top', transform=axTracesTW.transAxes, fontsize=largeFontSize)

    """
    patches = [mpatches.Patch(color='blue', label='Blue PM'),
               mpatches.Patch(color='green', label='Green PM'),
               mpatches.Patch(color='red', label='Red PM'),
               mpatches.Patch(color='m', label='Clockwise TW'),
               mpatches.Patch(color='black', label='Anti-clockwise TW'), ]
    plt.figure(fig.number)
    ax1RightTop.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=defaultFontSize)
    """
    plt.subplots_adjust(left=0., right=1., bottom=0., top=1.)
    margins(0, 0)
    gca().xaxis.set_major_locator(NullLocator())
    gca().yaxis.set_major_locator(NullLocator())
    fig.savefig('Image0_2.png', bbox_inches='tight', pad_inches=0)

    #plt.show()

####################### Cell3 image: IMAGE 0  end ###########################################################################


####################### Image1: 3D Visualization ###################################################
if 1 in ImagesToPlot:
    import matplotlib.image as mpimg

    print 'Generating Image1'
    figSizeX = paperWidthInInches;figSizeY = 0.6*paperWidthInInches;
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    ax = plt.subplot(111)


    ax.imshow(mpimg.imread("./image/4cell3D.png"))
    ax.text(0.039, 0.7, "${\Delta\\theta_{14}}$", ha='center', va='center', transform=ax.transAxes,
                     rotation=90, color='darkslategray', fontsize=largeFontSize, weight='roman')
    ax.text(0.09, 0.21, "${\Delta\\theta_{13}}$", ha='center', va='center', transform=ax.transAxes,
                     rotation=300, color='darkslategray', fontsize=largeFontSize, weight='roman')
    ax.text(0.59, 0.035, "${\Delta\\theta_{12}}$", ha='center', va='center', transform=ax.transAxes,
                     rotation=0.6, color='darkslategray', fontsize=largeFontSize, weight='roman')

    ax.text(0.039, 0.503, "$0.0$", ha='center', va='center', transform=ax.transAxes,
                     rotation=90, color='darkslategray', fontsize=defaultFontSize, weight='roman')
    ax.text(0.039, 0.9, "$1.0$", ha='center', va='center', transform=ax.transAxes,
                     rotation=90, color='darkslategray', fontsize=defaultFontSize, weight='roman')
    ax.text(0.054, 0.37, "$0.0$", ha='center', va='center', transform=ax.transAxes,
                     rotation=298, color='darkslategray', fontsize=defaultFontSize, weight='roman')
    ax.text(0.132, 0.075, "$1.0$", ha='center', va='center', transform=ax.transAxes,
                     rotation=300, color='darkslategray', fontsize=defaultFontSize, weight='roman')
    ax.text(0.18, 0.025, "$0.0$", ha='center', va='center', transform=ax.transAxes,
                     rotation=0, color='darkslategray', fontsize=defaultFontSize, weight='roman')
    ax.text(0.98, 0.07, "$1.0$", ha='center', va='center', transform=ax.transAxes,
                     rotation=0, color='darkslategray', fontsize=defaultFontSize, weight='roman')

    ax.plot(308.5, 741.6, 'o', color='white', ms=4)
    ax.plot(1525,455, 'o', color='white', ms=4)
    ax.plot(1640,1058, 'o', color='white', ms=4)
    ax.plot(2778,671.5, 'o', color='white', ms=4)

    ax.axis('off')
    fig.savefig('Image1.png', bbox_inches='tight', pad_inches=0)

####################### Image1 end: 3D Visualization ###################################################

####################### Network images: Image 2 ###############################################################################
if 2 in ImagesToPlot:

    print 'Generating Image2'
    figSizeX = 1.3*paperWidthInInches;
    figSizeY = 1.3*paperWidthInInches/4.;
    rows = 1;
    columns = 4;

    fig = plt.figure(figsize=(figSizeX, figSizeY))

    gs1 = gridspec.GridSpec(rows, columns)
    gs1.update(wspace=0.0, hspace=0.0)  # set the spacing between axes.

    ax0 = plt.subplot(gs1[0, 0])
    ax1 = plt.subplot(gs1[0, 1])
    ax2 = plt.subplot(gs1[0, 2])
    ax3 = plt.subplot(gs1[0, 3])

    ax0.set_xticks([]);
    ax0.set_yticks([]);
    ax1.set_xticks([]);
    ax1.set_yticks([])
    ax2.set_xticks([]);
    ax2.set_yticks([]);
    ax3.set_xticks([]);
    ax3.set_yticks([]);

    stretchFactor=1.
    drawNetwork(ax0, couplingStrengths=csChlorineOneWayClosedLoop, plotAxis=False, stretchFactor=stretchFactor)
    drawNetwork(ax1, couplingStrengths=csChlorineTwoWayClosedLoop, plotAxis=False, stretchFactor=stretchFactor)
    drawNetwork(ax2, couplingStrengths=csChlorineMixed_Rearranged, plotAxis=False, stretchFactor=stretchFactor)
    drawNetwork(ax3, couplingStrengths=csChlorineFullyConnected, plotAxis=False, stretchFactor=stretchFactor)

    xlim = [0.225, 0.775];
    ylim = xlim
    ax0.set_xlim(xlim);
    ax0.set_ylim(ylim)
    ax1.set_xlim(xlim);
    ax1.set_ylim(ylim)
    ax2.set_xlim(xlim);
    ax2.set_ylim(ylim)
    ax3.set_xlim(xlim);
    ax3.set_ylim(ylim)

    xLabelPos = 0.5;
    ylabelPos = 0
    ax0.text(xLabelPos, ylabelPos, '(a)', ha='center', va='top', transform=ax0.transAxes, fontsize=largeFontSize)
    ax1.text(xLabelPos, ylabelPos, '(b)', ha='center', va='top', transform=ax1.transAxes, fontsize=largeFontSize)
    ax2.text(xLabelPos, ylabelPos, '(c)', ha='center', va='top', transform=ax2.transAxes, fontsize=largeFontSize)
    ax3.text(xLabelPos, ylabelPos, '(d)', ha='center', va='top', transform=ax3.transAxes, fontsize=largeFontSize)

    plt.savefig('Image2.png', bbox_inches='tight')

# plt.show()
####################### Network images: Image2 end ###########################################################################



####################### Image3 - Bifurcations of symmetric fully connected network at eps 0.5 ########################

if 3 in ImagesToPlot:

    print 'Generating Image3'

    trajectoriesDataSaved = True; plotIvsG=True; plotDataSaved=True
    sweepResolution=25; cycles = 100
    processConverging = True; processNonconverging=False; minPercentForDBScan = 1

    csChlorine = csChlorineFullyConnected
    csChlorineName = 'csChlorineFullyConnected'
    eps = 0.5

    plotDataFile = None; fig = None
    if plotIvsG:
        figSizeX = 1.3*paperWidthInInches; figSizeY = 1.3*.6*paperWidthInInches; rows = 2; columns = 3;
        fig = plt.figure(figsize=(figSizeX, figSizeY))
        gs1 = gridspec.GridSpec(rows, columns)
        gs1.update(wspace=0.03, hspace=0.03)  # set the spacing between axes.

        #ax = fig.add_subplot(111, aspect=(.029 - .005) / (.575 - .4))
        ax = plt.subplot(gs1[:, 0:2])
        ax.set_xlim([0.003, 0.031])
        ax.set_ylim([0.3825, 0.5925])
        ax.set_xticks([0.005, 0.009, 0.013, 0.017, 0.021, 0.025, 0.029])
        ax.set_yticks([0.4, 0.435, 0.47, 0.505, 0.54, 0.575])
        ax.set_yticklabels(ax.get_yticks())
        for tick in ax.get_yticklabels():
            tick.set_rotation(90)
            tick.set_visible(True)
            tick.set_va('center')
        #ax.yaxis.set_tick_params(labelrotation=90)
        #ax.yaxis.get_ticklabels.set_va("center")
        #ax.yaxis.tick_right()
        #ax.xaxis.tick_top()

        if not plotDataSaved:
            plotDataFile = gzip.open(
                '4Cell_homogenous_dataOutput/4Cell_homogenous_plotData_eps_' + str(eps) + '_' + csChlorineName + '_sweepResolution_' + str(sweepResolution) + '_cycles_' + str(cycles) + '.pickle.gz',
                'wb')
        else:
            plotDataFile = gzip.open(
                '4Cell_homogenous_dataOutput/4Cell_homogenous_plotData_eps_' + str(eps) + '_' + csChlorineName + '_sweepResolution_' + str(sweepResolution) + '_cycles_' + str(cycles) + '.pickle.gz',
                'rb')

    print '\n\n----------------------------------------------------------------------------------------------------'
    print '----------------------------------------------------------------------------------------------------\n'+csChlorineName+ ' : \n', csChlorine

    for g in [0.005, 0.009, 0.013, 0.017, 0.021, 0.025, 0.029]: #np.arange(.005, 0.03, 0.004):
        for I in [0.4, 0.435, 0.47, 0.505, 0.54, 0.575]: #np.arange(.4, 0.58, 0.035):

            minSizeConvergingClustersDetails = None; pcConvergingAndClustered = 0
            if not plotDataSaved:
                if not trajectoriesDataSaved:
                    trajectoriesDataFile = gzip.open(
                        '4Cell_homogenous_dataOutput/4Cell_homogenous_eps_' + str(eps) + '_' +csChlorineName +'_g_' + str(g) + '_I_'+ str(I)  + '_sweepResolution_' + str(sweepResolution) + '_cycles_' + str(cycles) +  '.pickle.gz',
                        'wb')
                else:
                    trajectoriesDataFile = gzip.open(
                        '4Cell_homogenous_dataOutput/4Cell_homogenous_eps_' + str(eps) + '_' +csChlorineName +'_g_' + str(g) + '_I_'+ str(I)  + '_sweepResolution_' + str(sweepResolution) + '_cycles_' + str(cycles) +  '.pickle.gz',
                        'rb')

                print '\n\n\n---------------------------------------------------------------------------------------------------------'
                print csChlorineName, 'eps: ',eps, 'g : ', g, '; I : ', I

                if not trajectoriesDataSaved:
                    n = netw.network(networkSize, info=nf.info, coupling_strengths_chlorine=g * csChlorine,
                                     parameterSet=fh.parameters_generic(networkSize,
                                                                        [I, eps, 0., 10., -1.5, -1.2, 1.5, 1.]));
                    n.CYCLES = cycles
                    d, p, noP = sweepClusters.sweepTrajectoriesGPU(network=n, sweepResolution=sweepResolution)
                    pickle.dump(d, trajectoriesDataFile)
                    pickle.dump(p, trajectoriesDataFile)
                    pickle.dump(noP, trajectoriesDataFile)
                else:
                    d = pickle.load(trajectoriesDataFile)
                    p = pickle.load(trajectoriesDataFile)
                    noP = pickle.load(trajectoriesDataFile)
                trajectoriesDataFile.close()

                if processConverging:
                    d0 = d[d[:, 0] != -1]
                    d1 = sweepClusters.clusterize(d0, minClusterSize=1, roundingDecimals=2)
                    # In clusterize() method with  minClusterSize 50, we are ignoring points that are converging but are not part of large clusters. These points sometimes are too many to ignore
                    pcConvergingAndClustered = float(sum(d1[:, -1]))/len(d)
                    print 'Percentage converging : ' + str(float(len(d0)) / len(d))
                    print 'Percentage converging and clustered : ' + str(pcConvergingAndClustered)
                    minSizeConvergingClustersDetails  = sweepClusters.hierarchicalClustering(d1, networkSize=networkSize, phaseSpaceSize=sweepResolution ** 3, radius=0.15, minClusterPercent=0.00, roundingDecimals=2)

                if processNonconverging:
                    #In clusterize() method with  minClusterSize 50, we are ignoring points that are converging but are not part of large clusters. These points may be the ones near saddles. Should we include them? Including them by making minClusterSize=1 for now.
                    print 'Percentage non-converging : ' + str(1. - float(len(d0)) / len(d))
                    #noOfPhaseDifferencesToBackTrackForCycleDetection = 10;
                    if (1. - float(len(d0)) / len(d)) > minPercentForDBScan/100. :
                        d2 = []
                        for i in range(sweepResolution ** (networkSize-1)):
                            if (d[i][0] == -1):
                                # for j in range(noOfPhaseDifferencesToBackTrackForCycleDetection):
                                for j in range(int(noP[i])):
                                    d2.append(p[i][int(noP[i] - j)])
                        print len(d2)
                        # nClustersNonConverging = sweepClusters.clusterizeDBScanFull(d2, networkSize=networkSize, epsilon=0.001, minClusterSize=noOfPhaseDifferencesToBackTrackForCycleDetection*10)
                        #Here minClusterSize should be 1
                        d3 = sweepClusters.clusterize(d2, minClusterSize=1, roundingDecimals=2)
                        nClustersNonConverging = sweepClusters.clusterizeDBScan(d3, networkSize=networkSize, phaseSpaceSize=cycles*(25**3), epsilon=0.01,
                                                                                minClusterPercent=0.005)
                        # nClustersSaddles = sweepClusters.hierarchicalClustering(d3, networkSize=networkSize, radius=0.05)

            if plotIvsG:
                sizesOfDifferentSubregions = None
                variancesOfDifferentSubregions = None
                if not plotDataSaved:
                    sizesOfDifferentSubregions, variancesOfDifferentSubregions = sweepClusters.sizesAndVariancesOfDifferentAttractors(minSizeConvergingClustersDetails,pcConvergingAndClustered, maximumStandardClusterDifference=0.05)
                    pickle.dump(sizesOfDifferentSubregions, plotDataFile)
                    pickle.dump(variancesOfDifferentSubregions, plotDataFile)
                else:
                    sizesOfDifferentSubregions = pickle.load(plotDataFile)
                    variancesOfDifferentSubregions = pickle.load(plotDataFile)

                p = mpatches.Rectangle(
                    (g - 0.002, I - 0.0175), 0.004, 0.035, #fill=False,
                    facecolor='k',
                    edgecolor='k', linewidth=0  # Default
                )
                ax.add_patch(p)
                yMaxLastRatio = 0
                for i in range(len(sizesOfDifferentSubregions)):
                    noiseFunction = sweepClusters.plotPostProcessingClusterVariance(2.*variancesOfDifferentSubregions[i])
                    blockPercent = 0.95 # *0.98 to leave 2percent of each block as the boundary

                    if sizesOfDifferentSubregions[i] > 0.01:
                        patch = mpatches.Rectangle(
                            (g - 0.002, I - 0.0175 + 0.035 * yMaxLastRatio * blockPercent), 0.004* blockPercent,
                            0.035 * sizesOfDifferentSubregions[i] * blockPercent    ,
                            facecolor=sct.standardClusterProperties[i][2],
                            edgecolor=sct.standardClusterProperties[i][2], agg_filter=noiseFunction)
                        ax.add_patch(patch)
                        yMaxLastRatio += sizesOfDifferentSubregions[i]

    plotDataFile.close()
    if plotIvsG:
        legendPatches = [mpatches.Patch(color=sct.colorPH, label=sct.PH),
                   mpatches.Patch(color=sct.colorPM, label=sct.PM),
                   #mpatches.Patch(color=sct.a, label=sct.AP),
                   mpatches.Patch(color=sct.colorS, label=sct.S),
                   mpatches.Patch(color=sct.colorFT, label=sct.FT),
                   mpatches.Patch(color=sct.colorMT, label=sct.MT),
                   mpatches.Patch(color=sct.colorTr, label=sct.Tr),
                   mpatches.Patch(color=sct.colorNC, label=sct.NC), ]
        # put those patched as legend-handles ikto the legend
        #ax = plt.subplot(gs1[0, 2])
        plt.figure(fig.number)
        ax.legend(handles=legendPatches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=largeFontSize, fancybox=True, shadow=True)
        ax.text(0.82, 0.85, r'$\boldsymbol{A}$', ha='left', va='center', transform=ax.transAxes, fontsize=defaultFontSize, color='k')
        ax.text(0.82, 0.19, r'$\boldsymbol{B}$', ha='left', va='center', transform=ax.transAxes, fontsize=defaultFontSize, color='k')
        ax.text(0.96, 0.19, r'$\boldsymbol{C}$', ha='left', va='center', transform=ax.transAxes, fontsize=defaultFontSize, color='k')

        ax1 = plt.subplot(gs1[1, 2])
        drawNetwork(ax1, couplingStrengths=csChlorine, plotAxis=False, stretchFactor=1.5)

        xlabel = r'$ \mathrm{g_{inh}}$'
        ylabel = r'$ \mathrm{I_{app}}$'
        fig.text(0.42, 0.07, xlabel, ha='center', bbox=bbox_props_fig, color='darkslategray',
                 fontsize=largeFontSize,
                 weight='roman')
        fig.text(0.1, 0.49, ylabel, va='center', rotation='vertical', bbox=bbox_props_fig, color='darkslategray',
                 fontsize=largeFontSize, weight='roman')

        plt.savefig('Image3.png', bbox_inches='tight')


####################### Image3 End - Bifurcations of symmetric fully connected network at eps 0.5 ########################


####################### Image4 : Cell4 Different Trace patterns ###################################################
if 4 in ImagesToPlot:

    plotDataSaved = True
    labels = ['PH (0,0.5,0.5)', 'S (0,0,0)', 'FT (0.25,0.5,0.75)', 'PM (0.5,0.5,0.5)', 'MT (0.0,0.75,0.5)',
                  'Tr (0.41, 0.5, 0.91)', 'CM 1,2,3 vs. 4']

    label_desc = ['Paired halfcenter', 'Pacemaker', 'Synchronized state', 'Full travelling wave',
                  'Mixed travelling wave', 'Transition', 'Chimera (11:10)']
    tracesColors = [sct.colorPH, sct.colorPM, sct.colorS, sct.colorFT, sct.colorMT, sct.colorTr, sct.colorNC]
    params_I_cs_eps_ic = [(0.435, .009, 0.5, [0.5, 0.5, 0.0, 0]),
                          (0.4, .025, 0.5, [0.0, 0.5, 0.5, 0.5]) ,
                          (0.435, .013, 0.5, [0.0, 0.0, 0.0, 0]),
                          (0.505, .021, 0.5, [0., 0.25, 0.5, 0.75]),
                          (0.4, .005, 0.05, [0., 0.0, 0.75, 0.5]),
                          (0.552, .025, 0.5, [0, 0.25, 0.5, 0.75]),
                          (0.435, 0.029, 0.5, [0.04,0.03,0.02,0.01])]

    print 'Generating Image4'
    figSizeX = 1.3*paperWidthInInches; figSizeY = 1.3*paperWidthInInches*5./8;
    rows = 3; columns = 3;
    ax_arr = []

    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(rows, columns)
    gs1.update(wspace=0.05, hspace=0.18)  # set the spacing between axes.

    ax0 = plt.subplot(gs1[0, 0]); ax_arr.append(ax0)
    ax1 = plt.subplot(gs1[0, 1]); ax_arr.append(ax1)
    ax2 = plt.subplot(gs1[0, 2]); ax_arr.append(ax2)
    ax3 = plt.subplot(gs1[1, 0]); ax_arr.append(ax3)
    ax4 = plt.subplot(gs1[1, 1]); ax_arr.append(ax4)
    ax5 = plt.subplot(gs1[1, 2]); ax_arr.append(ax5)
    ax6 = plt.subplot(gs1[2, :]); ax_arr.append(ax6)

    all_axes = fig.get_axes()
    # show only the outside box as black borders and inside as darkgray
    for ax in all_axes:
        for sp in ax.spines.values():
            #sp.set_color('gray');
            sp.set_color('k');
        '''
        if ax.is_first_row():
            ax.spines['top'].set_color('k')
        if ax.is_last_row():
            ax.spines['bottom'].set_color('k')
        if ax.is_first_col():
            ax.spines['left'].set_color('k')
        if ax.is_last_col():
            ax.spines['right'].set_color('k')
        '''

    def get_network(I, g, eps=0.5):
        parameterSet = np.array([I, eps, 0., 10., -1.5, -1.2, 1.5, 1.,
                                 I, eps, 0., 10., -1.5, -1.2, 1.5, 1.,
                                 I, eps, 0., 10., -1.5, -1.2, 1.5, 1.,
                                 I, eps, 0., 10., -1.5, -1.2, 1.5, 1.])
        return netw.network(networkSize=4, parameterSet=parameterSet,
                            coupling_strengths_chlorine=g * csChlorineFullyConnected)

    if not plotDataSaved:
        pickleDataFile4 = gzip.open('Image4_4cellRhythms.gz', 'wb')
    else:
        pickleDataFile4 = gzip.open('Image4_4cellRhythms.gz', 'rb')

    bbox_props = dict(boxstyle="square,pad=0.2", fc="white", ec="k", lw=.5, alpha=.0)
    for j in range(7):
        params = params_I_cs_eps_ic[j]
        n = get_network(I=params[0], g=params[1], eps=params[2])
        if j==6:
            numCyclesToShow = 11; n.CYCLES = 107
        elif j==1:
            numCyclesToShow=3
        elif j==2:
            numCyclesToShow=2.25
        else:
            numCyclesToShow = 2.5

        if not plotDataSaved:
            Vx_i_new, d = computeTracesValues(n, n.load_initial_condition_generic(params[3]))
            pickle.dump(Vx_i_new, pickleDataFile4)
        else:
            Vx_i_new = pickle.load(pickleDataFile4)

        V_i_new = Vx_i_new[:, ::2]
        t = n.systemList[0].dt * np.arange(V_i_new.shape[0])

        curr_ax = ax_arr[j];
        if j==6:
            plotTraces(curr_ax, n, t, V_i_new, numCyclesToShow=numCyclesToShow, isMultiColored=False, monoColor=tracesColors[j], showBoxes=False)
            curr_ax.add_patch(patches.Rectangle((2210, -7.3), 10, 8, fc=sct.colorNC, alpha=0.4))
            curr_ax.add_patch(patches.Rectangle((2445, -7.3), 10, 8, fc=sct.colorNC, alpha=0.4))
            print curr_ax.get_xlim()
            print curr_ax.get_ylim()

        else:
            plotTraces(curr_ax, n, t, V_i_new, numCyclesToShow=numCyclesToShow, isMultiColored=False, monoColor=tracesColors[j])
        curr_ax.text(0.5, 1.02, label_desc[j], ha='center', va='bottom', transform=curr_ax.transAxes,
                     rotation=0, color=tracesColors[j], fontsize=largeFontSize, weight='roman')

        """
        xmin, xmax = curr_ax.get_xlim(); x_pad = (xmax - xmin) * 0.05;
        #curr_ax.set_xlim([xmin - x_pad, xmax + x_pad])
        ymin, ymax = curr_ax.get_ylim();  y_pad = (ymax - ymin) * 0.05;
        curr_ax.set_ylim([ymin - y_pad, ymax-y_pad])
        """
        curr_ax.set_xticks([]);curr_ax.set_yticks([])
        curr_ax.set_xticklabels([]);curr_ax.set_yticklabels([])
        #curr_ax.add_patch(Rectangle((xmin+0.1,ymin- y_pad),xmax-xmin, 1.2, fill=True, color='w', lw=1, zorder=3))
        #curr_ax.text(0.5, 0.03, labels[j], ha='center', transform=curr_ax.transAxes, fontsize=defaultFontSize, bbox=bbox_props, zorder=4)

    """
    patches = [mpatches.Patch(color=sct.sb, label=label_desc[0]),
               mpatches.Patch(color=sct.b, label=label_desc[1]),
               # mpatches.Patch(color=sct.a, label=sct.AP),
               mpatches.Patch(color=sct.g, label=label_desc[2]),
               mpatches.Patch(color=sct.p, label=label_desc[3]),
               mpatches.Patch(color=sct.dp, label=label_desc[4]),
               mpatches.Patch(color=sct.s, label=label_desc[5]),
               mpatches.Patch(color=sct.k, label=label_desc[6]), ]
    # put those patched as legend-handles into the legend
    # ax = plt.subplot(gs1[0, 2])
    plt.figure(fig.number)

    curr_ax.legend(handles=patches, ncol=4, bbox_to_anchor=(1.04, -0.1), borderaxespad=0.,
                   fontsize=defaultFontSize, fancybox=True, shadow=True)

    #curr_ax.legend(labels=label_desc, ncol=4, bbox_to_anchor=(1.04, -0.1), borderaxespad=0.,
    #               fontsize=defaultFontSize, handlelength=0., columnspacing=0)
    """

    fig.savefig('Image4.png', bbox_inches='tight')
    #plt.show()


####################### Image4 end: Cell4 Different Trace patterns ###################################################

####################### Image5 - Transitions - mono and bistable ########################
if 5 in ImagesToPlot:

    print 'Generating Image5'
    trajectoriesDataSaved = True; plotIvsG=True; plotDataSaved=True
    networkSize = 4; eps = 0.3
    g = 0.029; I = 0.54; sweepResolution=25; cycles = 100
    processConverging = True; processNonconverging=False; minPercentForDBScan = 1
    maximumStandardClusterDifference = 0.05         # For the standard pacemaker 0, 0, 0.5 this implies 0.95, 0.05, 0.55 is also considered the same cluster

    csChlorineSet = [csChlorineOneWayClosedLoop, csChlorineTwoWayClosedLoop, csChlorineMixed_Rearranged]
    csChlorineSetNames = [ 'csChlorineOneWayClosedLoop', 'csChlorineTwoWayClosedLoop', 'csChlorineMixed_Rearranged']
    if plotIvsG:
        figSizeX = 1.25*paperWidthInInches; figSizeY = 1.25*paperWidthInInches*0.5; rows = 3; columns = 6;
        fig = plt.figure(figsize=(figSizeX, figSizeY))
        gs1 = gridspec.GridSpec(rows, columns)
        gs1.update(wspace=0.03, hspace=0.1)  # set the spacing between axes.

    rowId = 0
    for comparisionBaseNetworkId in range(0, len(csChlorineSet)-1):
        print '\n\n----------------------------------------------------------------------------------------------------'
        print '\n\n----------------------------------------------------------------------------------------------------'
        print '----------------------------------------------------------------------------------------------------\n Comparisons with : ' + \
              csChlorineSetNames[comparisionBaseNetworkId]

        for comparisionObjectNetworkId in range(comparisionBaseNetworkId+1, len(csChlorineSet)):
            csChlorine = csChlorineSet[comparisionObjectNetworkId];
            csChlorineName = csChlorineSetNames[comparisionObjectNetworkId]
            print '\n\n----------------------------------------------------------------------------------------------------'
            print '----------------------------------------------------------------------------------------------------\n' + 'Transitions from ' + \
                  csChlorineSetNames[comparisionBaseNetworkId] + ' to ' + csChlorineName

            plotDataFile = None;

            if plotIvsG:

                ax = plt.subplot(gs1[rowId, 0])
                drawNetwork(ax, couplingStrengths=csChlorineSet[comparisionBaseNetworkId], plotAxis=False, stretchFactor=2., labelSize=largeFontSize)
                ax.text(-0.1, 0.5, '('+chr(rowId + ord('a'))+')', ha='center', va='top', transform=ax.transAxes,
                         fontsize=largeFontSize)

                ax = plt.subplot(gs1[rowId, 5])
                drawNetwork(ax, couplingStrengths=csChlorineSet[comparisionObjectNetworkId], plotAxis=False, stretchFactor=2., labelSize=largeFontSize)

                ax = plt.subplot(gs1[rowId, 1:5])
                #ax = fig.add_subplot(111, aspect=g / 4.)
                ax.set_xlim([-g / 20., g + g / 20.])
                ax.set_ylim([0., 1.])
                if rowId==0:
                    ax.xaxis.tick_top()
                    ax.set_xticks(np.arange(.0, g + 0.0000001, g / 10.))
                    ax.set_xticklabels(np.round(np.arange(0., 1.1, 0.1),1))
                else:
                    ax.set_xticks([])
                ax.set_yticks([])
                if not plotDataSaved:
                    plotDataFile = gzip.open(
                        '4Cell_homogenous_transitions_dataOutput/4Cell_homogenous_transitionsPlotData_eps_' + str(
                            eps) + '_from_' + csChlorineSetNames[
                            comparisionBaseNetworkId] + '_to_' + csChlorineName + '_sweepResolution_' + str(
                            sweepResolution) + '_cycles_' + str(cycles) + '.pickle.gz',
                        'wb')
                else:
                    plotDataFile = gzip.open(
                        '4Cell_homogenous_transitions_dataOutput/4Cell_homogenous_transitionsPlotData_eps_' + str(
                            eps) + '_from_' + csChlorineSetNames[
                            comparisionBaseNetworkId] + '_to_' + csChlorineName + '_sweepResolution_' + str(
                            sweepResolution) + '_cycles_' + str(cycles) + '.pickle.gz',
                        'rb')

            for gDiff in [0.0, 0.0029, 0.0058, 0.0087, 0.0116, 0.0145, 0.0174, 0.0203, 0.0232, 0.0261, 0.029]: #np.arange(.0, g + 0.0000001, g / 10.):
                minSizeConvergingClustersDetails = None;
                pcConvergingAndClustered = 0
                if not plotDataSaved:
                    if not trajectoriesDataSaved:
                        trajectoriesDataFile = gzip.open(
                            '4Cell_homogenous_transitions_dataOutput/4Cell_homogenous_eps_' + str(
                                eps) + '_' + csChlorineName + '_gDiff_' + str(gDiff) + '_' + csChlorineSetNames[
                                comparisionBaseNetworkId] + '_g_' + str(g) + '_I_' + str(I) + '_sweepResolution_' + str(
                                sweepResolution) + '_cycles_' + str(cycles) + '.pickle.gz',
                            'wb')
                    else:
                        trajectoriesDataFile = gzip.open(
                            '4Cell_homogenous_transitions_dataOutput/4Cell_homogenous_eps_' + str(
                                eps) + '_' + csChlorineName + '_gDiff_' + str(gDiff) + '_' + csChlorineSetNames[
                                comparisionBaseNetworkId] + '_g_' + str(g) + '_I_' + str(I) + '_sweepResolution_' + str(
                                sweepResolution) + '_cycles_' + str(cycles) + '.pickle.gz',
                            'rb')

                    print '\n\n\n---------------------------------------------------------------------------------------------------------'
                    print 'gDiff : ', gDiff

                    if not trajectoriesDataSaved:
                        n = netw.network(networkSize, info=nf.info, coupling_strengths_chlorine=g * csChlorineSet[
                            comparisionBaseNetworkId] + gDiff * (csChlorine - csChlorineSet[comparisionBaseNetworkId]),
                                         parameterSet=fh.parameters_generic(networkSize,
                                                                            [I, eps, 0., 10., -1.5, -1.2, 1.5, 1.]));
                        n.CYCLES = cycles
                        d, p, noP = sweepClusters.sweepTrajectoriesGPU(network=n, sweepResolution=sweepResolution)
                        pickle.dump(d, trajectoriesDataFile)
                        pickle.dump(p, trajectoriesDataFile)
                        pickle.dump(noP, trajectoriesDataFile)
                    else:
                        d = pickle.load(trajectoriesDataFile)
                        p = pickle.load(trajectoriesDataFile)
                        noP = pickle.load(trajectoriesDataFile)

                    # print d, p, noP

                    if processConverging:
                        d0 = d[d[:, 0] != -1]
                        d1 = sweepClusters.clusterize(d0, minClusterSize=1, roundingDecimals=2)
                        # In clusterize() method with  minClusterSize 50, we are ignoring points that are converging but are not part of large clusters. These points sometimes are too many to ignore
                        print 'Percentage converging : ' + str(float(len(d0)) / len(d))
                        print 'Percentage converging and clustered : ' + str(float(sum(d1[:, -1])) / len(d))
                        minSizeConvergingClustersDetails = sweepClusters.hierarchicalClustering(d1, networkSize=networkSize,
                                                                                                phaseSpaceSize=sweepResolution ** 3,
                                                                                                radius=0.15,
                                                                                                minClusterPercent=0.00,
                                                                                                roundingDecimals=2)

                    if processNonconverging:
                        # In clusterize() method with  minClusterSize 50, we are ignoring points that are converging but are not part of large clusters. These points may be the ones near saddles. Should we include them?
                        print 'Percentage non-converging : ' + str(1. - float(len(d0)) / len(d))
                        # noOfPhaseDifferencesToBackTrackForCycleDetection = 10;
                        if (1. - float(len(d0)) / len(d)) > minPercentForDBScan / 100.:
                            d2 = []
                            for i in range(sweepResolution ** (networkSize - 1)):
                                if (d[i][0] == -1):
                                    # for j in range(noOfPhaseDifferencesToBackTrackForCycleDetection):
                                    for j in range(int(noP[i])):
                                        d2.append(p[i][int(noP[i] - j)])
                            print len(d2)
                            # nClustersNonConverging = sweepClusters.clusterizeDBScanFull(d2, networkSize=networkSize, epsilon=0.001, minClusterSize=noOfPhaseDifferencesToBackTrackForCycleDetection*10)
                            # Here minClusterSize should be 1
                            d3 = sweepClusters.clusterize(d2, minClusterSize=1, roundingDecimals=2)
                            nClustersNonConverging = sweepClusters.clusterizeDBScan(d3, networkSize=networkSize,
                                                                                    phaseSpaceSize=cycles * (25 ** 3),
                                                                                    epsilon=0.01,
                                                                                    minClusterPercent=0.005)
                            # nClustersSaddles = sweepClusters.hierarchicalClustering(d3, networkSize=networkSize, radius=0.05)

                if plotIvsG:
                    sizesOfDifferentSubregions = None
                    variancesOfDifferentSubregions = None
                    if not plotDataSaved:
                        sizesOfDifferentSubregions, variancesOfDifferentSubregions = sweepClusters.sizesAndVariancesOfDifferentAttractors(
                            minSizeConvergingClustersDetails, pcConvergingAndClustered,
                            maximumStandardClusterDifference=0.05)
                        pickle.dump(sizesOfDifferentSubregions, plotDataFile)
                        pickle.dump(variancesOfDifferentSubregions, plotDataFile)
                    else:
                        sizesOfDifferentSubregions = pickle.load(plotDataFile)
                        variancesOfDifferentSubregions = pickle.load(plotDataFile)

                    p = mpatches.Rectangle(
                        (gDiff - g / 20., 0), g / 10., 1., #fill=False,
                        facecolor='k',
                        edgecolor='k', linewidth=5  # Default
                    )
                    ax.add_patch(p)
                    yMaxLastRatio = 0
                    for i in range(len(sizesOfDifferentSubregions)):
                        noiseFunction = sweepClusters.plotPostProcessingClusterVariance(variancesOfDifferentSubregions[i])
                        patch = mpatches.Rectangle(
                            (gDiff - g / 20., 0 + 1. * yMaxLastRatio), g / 10.,
                                                                       1. * sizesOfDifferentSubregions[i],
                            facecolor=sct.standardClusterProperties[i][2],
                            edgecolor=sct.standardClusterProperties[i][2])#, agg_filter=noiseFunction)
                        ax.add_patch(patch)
                        yMaxLastRatio += sizesOfDifferentSubregions[i]
            rowId=rowId+1;

    if plotIvsG:
        legendPatches = [#mpatches.Patch(color=sct.g, label=sct.S),
                   #mpatches.Patch(color=sct.b, label=sct.P),
                   #mpatches.Patch(color=sct.a, label=sct.AP),
                   mpatches.Patch(color=sct.colorPH, label='Paired halfcenter (0.5, 0, 0.5)'),
                   mpatches.Patch(color=sct.colorFT, label='Full travelling wave (0.25, 0.5, 0.75)'),]
                   #mpatches.Patch(color=sct.dp, label=sct.MT),
                   #mpatches.Patch(color=sct.s, label=sct.Tr),
                   #mpatches.Patch(color=sct.w, label=sct.NC), ]
        # put those patched as legend-handles into the legend
        plt.figure(fig.number)
        plt.legend(handles=legendPatches, bbox_to_anchor=(-0.25, -0.2, 1.5, .102), loc='upper left', ncol=2, mode="expand",
                   borderaxespad=0., fancybox=True, shadow=True, fontsize=largeFontSize,) #bbox_to_anchor=[x0, y0, width, height]
        xlabel = r'$ \mathrm{\frac{g_{inh}^{MissingConnections}}{g_{inh}^{Network}}=}$';
        fig.text(0.185, 0.883, xlabel, ha='center', bbox=bbox_props_index, color='darkslategray',
                 fontsize=indexFontSize,
                 weight='roman')
        plt.savefig('Image5.png', bbox_inches='tight')

####################### Image5 End - Transitions - mono and bistable ########################


####################### Image6_0 Start ########################

if 6 in ImagesToPlot:

    print 'Generating Image6'
    plotDataSaved=True
    figSizeX = 1.3*paperWidthInInches/2.; figSizeY = 1.3*paperWidthInInches/2.; rows = 1; columns = 1;
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(rows, columns)
    gs1.update(wspace=0.03, hspace=0.1)  # set the spacing between axes.

    ax = plt.subplot(gs1[0, 0])

    networkSize = 4
    cs = 0.025 * np.asarray(np.matrix("0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0"))
    parameterSet = fh.parameters_generic(networkSize, [0.435, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])
    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=cs)
    n.CYCLES = 100

    if not plotDataSaved:
        pickleDataFile0 = gzip.open('Image6_0.gz', 'wb')
    else:
        pickleDataFile0 = gzip.open('Image6_0.gz', 'rb')

    Vx_i_new_1 , Vx_i_new_2 = None, None
    if not plotDataSaved:
        # Sync state
        Vx_i_new_1, d_1 = computeTracesValues(n, initial_condition=n.load_initial_condition_generic(
            [0.02, 0.015, 0.01, 0.005]))
        # PH
        Vx_i_new_2, d_2 = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.,0.05,0.5,.51]));

        pickle.dump([Vx_i_new_1, Vx_i_new_2], pickleDataFile0)

    else:
        [Vx_i_new_1, Vx_i_new_2] = pickle.load(pickleDataFile0)

    """
    V_i_new_1 = Vx_i_new_1[:, ::2]
    t_1 = n.systemList[0].dt * np.arange(V_i_new_1.shape[0])
    V_i_new_2 = Vx_i_new_2[:, ::2]
    t_2 = n.systemList[0].dt * np.arange(V_i_new_2.shape[0])
    """

    ###Actual Plot#########
    # Plot System
    plotSystem(ax, n, Vx_i_new_1, saveAnimation=False, indexToPlot=400, plotPast=False, pastPercent=0.1, pastNumber=30, noOfCyclesToAnimate=20, plotTransient=True, plotOrbit=True, orbitColor=sct.colorS, noOfCyclesInOrbit=1, showColoredCells=False, showCellLabels=False)
    plotSystem(ax, n, Vx_i_new_2, saveAnimation=False, indexToPlot=149, plotPast=False, pastPercent=0.1, pastNumber=30, noOfCyclesToAnimate=20, plotTransient=True, plotOrbit=True, orbitColor=sct.colorPH, noOfCyclesInOrbit=1, showColoredCells=False, showCellLabels=False)
    ax.set_xticks([]);
    ax.set_yticks([]);
    ax.set_xlim(-0.05, 1.05)
    ax.set_ylim(-1.5, 1.5)
    ax.text(0.5, -0.04, r'Recovery $x$' , ha='center', va='center', transform=ax.transAxes,
                 rotation=0, color='darkslategray', fontsize=defaultFontSize, weight='roman')
    ax.text(1., -0.04, r'$1.0$', ha='right', va='center', transform=ax.transAxes,
                 fontsize=defaultFontSize, rotation=0)
    ax.text(0., -0.04, r'$0.0$', ha='left', va='center', transform=ax.transAxes,
                 fontsize=defaultFontSize, rotation=0)
    ax.text(-0.03, 0.52, r'Voltage $V$', ha='center', va='center', transform=ax.transAxes,
                 rotation=90, color='darkslategray', fontsize=defaultFontSize, weight='roman')
    ax.text(-0.03, 0., r'$-1.5$', ha='center', va='bottom', transform=ax.transAxes,
                 fontsize=defaultFontSize, rotation=90)
    ax.text(-0.03, 1., r'$1.5$', ha='center', va='top', transform=ax.transAxes,
                 fontsize=defaultFontSize, rotation=90)
    ax.text(0.025, 0.52, r'$V_{th}=0$', ha='left', va='center', transform=ax.transAxes,
                 fontsize=defaultFontSize)

    ax.annotate(r'$\frac{dV}{dt}=0$', xy=(0, 1.2), xytext=(0, 1.2), bbox=bbox_props, color='k', rotation=-8, fontsize=largeFontSize)
    ax.annotate(r'$\frac{dx}{dt}=0$', xy=(0.93, 1.15), xytext=(0.93, 1.15), bbox=bbox_props, color='k', rotation=90, fontSize=largeFontSize)

    fig.savefig('Image6.png', bbox_inches='tight')

######################### Image6_0 End ########################


####################### Image6_1 Start ########################

if 61 in ImagesToPlot:

    print 'Generating Image6_1'
    plotDataSaved=False

    figSizeX = 10; figSizeY = 3; rows = 2; columns = 5;
    fig = plt.figure(figsize=(figSizeX, figSizeY))
    gs1 = gridspec.GridSpec(rows, columns)
    gs1.update(wspace=0.03, hspace=0.1)  # set the spacing between axes.

    ax = plt.subplot(gs1[0, 0])
    ax1 = plt.subplot(gs1[0, 1:4])
    ax2 = plt.subplot(gs1[0, 4])

    ax3 = plt.subplot(gs1[1,0])
    ax4 = plt.subplot(gs1[1,1])
    ax5 = plt.subplot(gs1[1,2])
    ax6 = plt.subplot(gs1[1,3])
    ax7 = plt.subplot(gs1[1,4])

    cs = np.asarray(np.matrix("0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0"))
    networkSize = 4

    if not plotDataSaved:
        pickleDataFile0 = gzip.open('Image6_1.gz', 'wb')
    else:
        pickleDataFile0 = gzip.open('Image6_1.gz', 'rb')


    ############ Sync ########
    I_app = 0.435; g = 0.025;
    parameterSet = fh.parameters_generic(networkSize, [I_app, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])
    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=g*cs)
    n.CYCLES = 100

    Vx_i_new_1, Vx_i_new_2 = None, None
    if not plotDataSaved:
        Vx_i_new, d = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.04,0.03,0.02,0.01]))
        pickle.dump( Vx_i_new, pickleDataFile0)
    else:
        Vx_i_new = pickle.load(pickleDataFile0)

    V_i_new = Vx_i_new[:,::2]
    t = n.systemList[0].dt * np.arange(V_i_new.shape[0])
    plotTraces(ax, n, t, V_i_new, numCyclesToShow=2.25)

    ax.set_xticks([]);
    ax.set_yticks([]);

    ############ Chimera ########
    I_app = 0.435; g = 0.029;
    parameterSet = fh.parameters_generic(networkSize, [I_app, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])
    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=g*cs)
    n.CYCLES = 107

    Vx_i_new_1, Vx_i_new_2 = None, None
    if not plotDataSaved:
        Vx_i_new, d = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.04,0.03,0.02,0.01]))
        pickle.dump( Vx_i_new, pickleDataFile0)
    else:
        Vx_i_new = pickle.load(pickleDataFile0)

    V_i_new = Vx_i_new[:,::2]
    t = n.systemList[0].dt * np.arange(V_i_new.shape[0])
    plotTraces(ax1, n, t, V_i_new, numCyclesToShow=11)

    ax1.set_xticks([]);
    ax1.set_yticks([]);

    ############ PM ########
    I_app = 0.435; g = 0.033;
    parameterSet = fh.parameters_generic(networkSize, [I_app, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])
    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=g*cs)
    n.CYCLES = 100

    Vx_i_new_1, Vx_i_new_2 = None, None
    if not plotDataSaved:
        Vx_i_new, d = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.04,0.03,0.02,0.01]))
        pickle.dump( Vx_i_new, pickleDataFile0)
    else:
        Vx_i_new = pickle.load(pickleDataFile0)

    V_i_new = Vx_i_new[:,::2]
    t = n.systemList[0].dt * np.arange(V_i_new.shape[0])
    plotTraces(ax2, n, t, V_i_new, numCyclesToShow=2)

    ax2.set_xticks([]);
    ax2.set_yticks([]);

    ############ Trnstn 1 ########
    I_app = 0.55; g = 0.025;
    parameterSet = fh.parameters_generic(networkSize, [I_app, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])
    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=g*cs)
    n.CYCLES = 100

    Vx_i_new_1, Vx_i_new_2 = None, None
    if not plotDataSaved:
        Vx_i_new, d = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.,0.25,0.5,.75]))
        pickle.dump( Vx_i_new, pickleDataFile0)
    else:
        Vx_i_new = pickle.load(pickleDataFile0)

    V_i_new = Vx_i_new[:,::2]
    t = n.systemList[0].dt * np.arange(V_i_new.shape[0])
    plotTraces(ax3, n, t, V_i_new, numCyclesToShow=2)

    ax3.set_xticks([]);
    ax3.set_yticks([]);

    ############ Trnstn 2 ########
    I_app = 0.552; g = 0.025;
    parameterSet = fh.parameters_generic(networkSize, [I_app, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])
    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=g*cs)
    n.CYCLES = 100

    Vx_i_new_1, Vx_i_new_2 = None, None
    if not plotDataSaved:
        Vx_i_new, d = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.,0.25,0.5,.75]))
        pickle.dump( Vx_i_new, pickleDataFile0)
    else:
        Vx_i_new = pickle.load(pickleDataFile0)

    V_i_new = Vx_i_new[:,::2]
    t = n.systemList[0].dt * np.arange(V_i_new.shape[0])
    plotTraces(ax4, n, t, V_i_new, numCyclesToShow=2)

    ax4.set_xticks([]);
    ax4.set_yticks([]);

    ############ Trnstn 3 ########
    I_app = 0.554; g = 0.025;
    parameterSet = fh.parameters_generic(networkSize, [I_app, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])
    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=g*cs)
    n.CYCLES = 100

    Vx_i_new_1, Vx_i_new_2 = None, None
    if not plotDataSaved:
        Vx_i_new, d = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.,0.25,0.5,.75]))
        pickle.dump( Vx_i_new, pickleDataFile0)
    else:
        Vx_i_new = pickle.load(pickleDataFile0)

    V_i_new = Vx_i_new[:,::2]
    t = n.systemList[0].dt * np.arange(V_i_new.shape[0])
    plotTraces(ax5, n, t, V_i_new, numCyclesToShow=2)

    ax5.set_xticks([]);
    ax5.set_yticks([]);

    ############ Trnstn 4 ########
    I_app = 0.556; g = 0.025;
    parameterSet = fh.parameters_generic(networkSize, [I_app, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])
    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=g*cs)
    n.CYCLES = 100

    Vx_i_new_1, Vx_i_new_2 = None, None
    if not plotDataSaved:
        Vx_i_new, d = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.,0.25,0.5,.75]))
        pickle.dump( Vx_i_new, pickleDataFile0)
    else:
        Vx_i_new = pickle.load(pickleDataFile0)

    V_i_new = Vx_i_new[:,::2]
    t = n.systemList[0].dt * np.arange(V_i_new.shape[0])
    plotTraces(ax6, n, t, V_i_new, numCyclesToShow=2)

    ax6.set_xticks([]);
    ax6.set_yticks([]);

    ############ Trnstn 5 ########
    I_app = 0.558; g = 0.025;
    parameterSet = fh.parameters_generic(networkSize, [I_app, 0.5, 0., 10., -1.5, -1.2, 1.5, 1.])
    n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=g*cs)
    n.CYCLES = 100

    Vx_i_new_1, Vx_i_new_2 = None, None
    if not plotDataSaved:
        Vx_i_new, d = computeTracesValues(n, initial_condition=n.load_initial_condition_generic([0.,0.25,0.5,.75]))
        pickle.dump( Vx_i_new, pickleDataFile0)
    else:
        Vx_i_new = pickle.load(pickleDataFile0)

    V_i_new = Vx_i_new[:,::2]
    t = n.systemList[0].dt * np.arange(V_i_new.shape[0])
    plotTraces(ax7, n, t, V_i_new, numCyclesToShow=2)

    ax7.set_xticks([]);
    ax7.set_yticks([]);

    ##########################
    fig.savefig('Image6_1.png', bbox_inches='tight')

####################### Image6_1 End ########################



