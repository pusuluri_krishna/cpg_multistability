from __future__ import division
import sys
sys.path.insert(0, '../Tools')
import time
import info as nf
import numpy as np
import Network_Generic_allSynapseTypes as netw
import fitzhugh_allSynapseTypes as fh
from sklearn.cluster import DBSCAN
import scipy.stats
import networkx as nx
import scipy.cluster.hierarchy as hac
from compiler.ast import flatten
import random
import standardClusterTable_4Cell as sct
#from skimage.util import random_noise


dataSaved = True

def clusterize(d1, minClusterSize = 1, roundingDecimals=2):
    print 'In preliminary clusterize method; minClusterSize : ', minClusterSize, '; roundingDecimals : ', roundingDecimals
    d = np.round(d1,roundingDecimals);
    d[d==1.] = 0.
    def uniq(lst):
        #last = object()
        last = None
        i = 0
        beforeLast = None
        for item in lst:
            if item == last:
                i=i+1;
                continue
            if(last!=None):
                if(i>=minClusterSize):
                    last.append(i)
                    yield last
            last = item
            i=1
        if(i>=minClusterSize):
            last.append(i)
            yield last
    def sort_and_deduplicate(l):
        return list(uniq(sorted(l, reverse=True)))
    #print d.tolist()
    #print tuple(d.tolist())
    #clusters = set(tuple(d.tolist()))
    clusters = sort_and_deduplicate(d.tolist())
    print 'Preliminary number of clusters in clusterize : ', len(clusters), '\n Preliminary clusters : '
    #print clusters
    return np.array(clusters)

def distanceMetric(d1, d2):
    sqdistance = 0
    for i in range(len(d1)):
        x1 = d1[i]; x2 = d2[i]
        sqdistance += min(abs(x1-x2), 1-abs(x1-x2))**2
    return sqdistance

def weightedClusterMeanAndSpread(cluster):
    repeatedRow = cluster[0]
    data = np.array([repeatedRow[:-1], ] * int(repeatedRow[-1]))
    for repeatedRow in cluster[1:]:
        data = np.concatenate((data, np.array([repeatedRow[:-1], ] * int(repeatedRow[-1]))))
    return clusterMeanAndSpread(cluster[:, :-1])

#can be run on the output of clusterize()
def clusterizeDBScan(d, networkSize, phaseSpaceSize, minClusterSize = 1, epsilon = 0.02, roundingDecimals = 2, minClusterPercent = 0.01):
    #http://scikit-learn.org/stable/modules/clustering.html#dbscan
    #http://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html#sphx-glr-auto-examples-cluster-plot-dbscan-py
    #Half moons : https://blog.dominodatalab.com/topology-and-density-based-clustering/
    #http://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_comparison.html#sphx-glr-auto-examples-cluster-plot-cluster-comparison-py
    #http://geoffboeing.com/2014/08/clustering-to-reduce-spatial-data-set-size/
    #parallel mapreduce : http://geoffboeing.com/2014/08/clustering-to-reduce-spatial-data-set-size/
    #parallel gpu : http://www.sciencedirect.com/science/article/pii/S1877050913003438
    #https://github.com/lucerne/GPU-machine-learning
    #visualize : http://stats.stackexchange.com/questions/52625/visually-plotting-multi-dimensional-cluster-data
    print 'In DBScan method ; minClusterSize : ', minClusterSize, '; epsilon : ', epsilon
    db = DBSCAN(eps=(epsilon**2)*(networkSize-1), min_samples=minClusterSize, metric=distanceMetric).fit(d[:,:-1])
    labels = db.labels_
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    #first cluster is X[labels == 0]
    clusters = [d[labels == i] for i in xrange(n_clusters_)]
    """
    #outliers have label -1
    outliers = d[labels == -1]
    print 'The merged clusters  are : '
    for cluster in clusters :
        size = sum(cluster[:,-1])
        mode = cluster[cluster[:,-1]==max(cluster[:,-1])][0]
        mode = [round(element, 2) for element in mode]
        print mode[:-1] , '; size : ', size
    print 'Estimated number of clusters after DBScan: %d' % n_clusters_
    return n_clusters_
    """
    nClustersMinSize = 0
    for cluster in clusters:
        clusterMean, clusterStd = weightedClusterMeanAndSpread(cluster)
        #clusterPercent = np.round(sum(cluster[:, -1]) / sum(d[:, -1]), roundingDecimals)
        clusterPercent = np.round(sum(cluster[:, -1]) / phaseSpaceSize, roundingDecimals)
        if(clusterPercent >= minClusterPercent):
            print np.round(clusterMean, roundingDecimals), '\t\t', np.round(clusterStd, roundingDecimals), '\t\t', clusterPercent
            nClustersMinSize = nClustersMinSize+1
    print 'Number of Clusters : ', nClustersMinSize
    print '\n\n'
    return nClustersMinSize

def clusterMeanAndSpread(x):
    #we need circular mean and variance : https://en.wikipedia.org/wiki/Mean_of_circular_quantities
    circularMean = []
    circularSpread = []
    for t in x.T:
        circularMean.append(scipy.stats.circmean(t, 1., 0.))
        circularSpread.append(scipy.stats.circstd(t, 1., 0.))
    # spread from first element -- np.mean(np.minimum.reduce([abs(x-x[0]), 1-abs(x-x[0])]),0) -- not exactly what we need here
    return np.array(circularMean), np.array(circularSpread)

#can be run by itself
def clusterizeDBScanFull(d, networkSize, phaseSpaceSize, minClusterSize = 1, roundingDecimals = 2, epsilon = 0.02):
    #http://scikit-learn.org/stable/modules/clustering.html#dbscan
    #http://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html#sphx-glr-auto-examples-cluster-plot-dbscan-py
    #Half moons : https://blog.dominodatalab.com/topology-and-density-based-clustering/
    #http://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_comparison.html#sphx-glr-auto-examples-cluster-plot-cluster-comparison-py
    #http://geoffboeing.com/2014/08/clustering-to-reduce-spatial-data-set-size/
    #parallel mapreduce : http://geoffboeing.com/2014/08/clustering-to-reduce-spatial-data-set-size/
    #parallel gpu : http://www.sciencedirect.com/science/article/pii/S1877050913003438
    #https://github.com/lucerne/GPU-machine-learning
    #visualize : http://stats.stackexchange.com/questions/52625/visually-plotting-multi-dimensional-cluster-data
    d = np.round(d, roundingDecimals);
    print '\n In Full DBScan method ; minClusterSize : ', minClusterSize, '; epsilon : ', epsilon
    db = DBSCAN(eps=(epsilon**2)*(networkSize-1), min_samples=minClusterSize, metric=distanceMetric).fit(d)
    labels = db.labels_
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    #first cluster is X[labels == 0]
    clusters = [d[labels == i] for i in xrange(n_clusters_)]
    #outliers have label -1
    outliers = d[labels == -1]
    print 'The dbscan clusters are : \n [CircularMeans]\t\t[CircularStdDeviations]\t\tClusterSizeRatioInPhaseSpace'
    for cluster in clusters :
        clusterMean, clusterStd = clusterMeanAndSpread(cluster)
        #print np.round(clusterMean, roundingDecimals), '\t\t', np.round(clusterStd, roundingDecimals), '\t\t', np.round(len(cluster)/len(d),roundingDecimals)
        print np.round(clusterMean, roundingDecimals), '\t\t', np.round(clusterStd, roundingDecimals), '\t\t', np.round(len(cluster)/phaseSpaceSize,roundingDecimals)
    print 'Estimated number of clusters after DBScan: %d' % n_clusters_
    return n_clusters_

def clusterizeWithCliques(d, networkSize, phaseSpaceSize, epsilon,roundingDecimals):
    print 'In clusterizeWithCliques method ; epsilon : ', epsilon, '; roundingDecimals : ', roundingDecimals
    l = len(d)
    connectivityMatrix = np.zeros([l,l])
    for i in range(l) :
        for j in range(i,l) :
            if distanceMetric(d[i,:-1],d[j,:-1])<(epsilon**2)*(networkSize-1) :
                connectivityMatrix[i,j]=1
                connectivityMatrix[j,i]=1

    G = nx.from_numpy_matrix(connectivityMatrix)
    #https://networkx.github.io/documentation/networkx-1.9.1/reference/generated/networkx.algorithms.clique.find_cliques.html
    cliques = list(nx.find_cliques(G))
    print "Indices of cluster cliques : ", cliques
    l1 = len(flatten(cliques))
    print 'Estimated number of clusters : ', len(cliques), '; Sum of nodes in all cliques :', l1
    print 'The identified clusters using cliques are : \n [CircularMeans]\t\t[CircularStdDeviations]\t\tClusterSizeRatioInPhaseSpace'
    #assert l == l1  #It is possible for a node to be present in two different cliques; then l1 > l since we are counting same rows multiple times. In that case clusters also won't be accurate
    if(l!=l1): print 'ATTENTION : Intersecting clusters'
    clusters = [d[list(x)] for x in cliques]
    for cluster in clusters:
        repeatedRow = cluster[0]
        data = np.array([repeatedRow[:-1], ] * repeatedRow[-1])
        for repeatedRow in cluster[1:]:
            data=np.concatenate((data,np.array([repeatedRow[:-1], ] * repeatedRow[-1])))
        clusterMean, clusterStd = clusterMeanAndSpread(cluster[:,:-1])
        #print np.round(clusterMean, roundingDecimals), '\t\t', np.round(clusterStd, roundingDecimals), '\t\t', np.round(len(cluster) / len(d), roundingDecimals)
        print np.round(clusterMean, roundingDecimals), '\t\t', np.round(clusterStd, roundingDecimals), '\t\t', np.round(len(cluster) / phaseSpaceSize, roundingDecimals)
    print '\n\n'
    return len(clusters)

def hierarchicalClustering(data, networkSize, phaseSpaceSize, radius=0.1, roundingDecimals=2, minClusterPercent = 0.005):
    print 'In hierarchical clustering method'
    if(len(data)<1):
        print 'Warning: Data size less than 1; No clusters returned'
        print data
        return
    elif(len(data)==1):
	clusters = [data]
    else: 
        data1 = data[:,:-1]
        z = hac.linkage(data1, metric=distanceMetric, method='complete')
        labels = hac.fcluster(z, (radius**2)*(networkSize-1), 'distance')
        clusters = [data[labels == i] for i in xrange(1, max(labels)+1)]
    nMinSizeClusters = 0
    minSizeClustersDetails = []
    weightedAvgClusterStdDevForNoise = [0,0,0]
    for cluster in clusters:
        clusterMean, clusterStd = weightedClusterMeanAndSpread(cluster)
        #clusterPercent = sum(cluster[:, -1]) / sum(data[:, -1])
        clusterPercent = sum(cluster[:, -1]) / phaseSpaceSize
        if(clusterPercent >= minClusterPercent):
            print np.round(clusterMean, roundingDecimals), '\t\t', np.round(clusterStd, roundingDecimals), '\t\t', clusterPercent
            #print clusterMean, '\t\t', clusterStd, '\t\t', clusterPercent
            minSizeClustersDetails.append(np.array([clusterMean, clusterStd, clusterPercent]))
            nMinSizeClusters = nMinSizeClusters+1
            weightedAvgClusterStdDevForNoise += clusterStd*clusterPercent
    print 'Number of Clusters: ', nMinSizeClusters
    print 'Weighted Avg. Circular Std Dev. of all Clusters: ', np.mean(weightedAvgClusterStdDevForNoise)
    print '\n\n'
    return np.array(minSizeClustersDetails)

def sweepTrajectoriesGPU(network, sweepResolution=5):
    sweepResolutionFloat = float(sweepResolution);
    initial_condition_array = []
    initial_condition_array_size = sweepResolution**(network.networkSize-1)
    print 'networkSize : ', network.networkSize, '; cycles : ', network.CYCLES, '; sweepResolution : ', sweepResolution
    print 'Computing initial conditions array'
    for i in range(initial_condition_array_size):
        initialPhases = [0]
        temp = i
        for j in reversed(range(network.networkSize-1)) :
            initialPhases.append((temp/(sweepResolution**j))/sweepResolutionFloat)
            temp = temp%(sweepResolution**j)
        initial_condition = network.load_initial_condition_generic(initialPhases)
        initial_condition_array.extend(initial_condition)
    print 'Beginning sweep on GPU'
    startTime = time.time();
    # no.of cycles is based on last neuron
    d, p, noP = fh.sweeptraces_electricExcitatoryAndInhibitoryConnections(network.networkSize, initial_condition_array,
                                                                  initial_condition_array_size,
                                                                  network.coupling_strengths_chlorine,
                                                                  network.coupling_strengths_potassium,
                                                                  network.coupling_strengths_sodium,
                                                                  network.coupling_strengths_electric,
                                                                  network.systemList[0].dt /
                                                                  float(network.systemList[0].stride),
                                                                  network.systemList[0].N_output(
                                                                      network.CYCLES),
                                                                  network.CYCLES,
                                                                  network.systemList[0].stride,
                                                                  network.parameterSet,
                                                                  withFixedPointDetection=True)
    endTime = time.time();
    print("Time taken in seconds for sweep :", endTime - startTime);
    return d, p, noP

#takes as input the clusterDetails returned from hierarchical clustering method and returns the sizes and variances of different standard clusters; maximumStandardClusterDifference = 0.05 - for the standard pacemaker 0, 0, 0.5 this implies 0.95, 0.05, 0.55 is also considered the same cluster
def sizesAndVariancesOfDifferentAttractors(minSizeConvergingClustersDetails, pcConvergingAndClustered, maximumStandardClusterDifference=0.05):
    sizesOfDifferentSubregions = np.zeros(
        len(sct.standardClusterTable) + 2)  # all standard clusters + transitions + non-convergences
    sizesOfDifferentSubregions[-1] = 1 - pcConvergingAndClustered  # non-convergence ratio
    variancesOfDifferentSubregions = np.zeros(len(
        sct.standardClusterTable) + 2);  # varianceSum of each standard cluster + varianceSum of transitions + no variance for non-convergence
    variancesOfDifferentSubregions[-1] = 0
    for cluster in minSizeConvergingClustersDetails:
        clusterMean = cluster[0]
        clusterType = 'Transitions';
        clusterIdentifier = 0;
        clusterColor = 'red'
        clusterIndex = len(sct.standardClusterTable)
        for i in range(len(sct.standardClusterTable)):
            # if sweepClusters.distanceMetric(clusterMean, sct.standardClusterTable[i]) < (networkSize -1)*maximumStandardClusterDifference**2 :
            #    clusterIndex = i
            if distanceMetric([clusterMean[0]], [sct.standardClusterTable[i][
                                                                   0]]) < maximumStandardClusterDifference ** 2 and distanceMetric(
                [clusterMean[1]], [sct.standardClusterTable[i][
                                       1]]) < maximumStandardClusterDifference ** 2 and distanceMetric(
                [clusterMean[2]],
                [sct.standardClusterTable[i][2]]) < maximumStandardClusterDifference ** 2:
                clusterIndex = i;
                break
        sizesOfDifferentSubregions[clusterIndex] += cluster[2]
        variancesOfDifferentSubregions[clusterIndex] += abs(sum(cluster[1]))
    # not considering the variance withing transitions, as that seems to be much more than the rest and the plot doesn't look good at transitions
    variancesOfDifferentSubregions[-2] = 0
    return sizesOfDifferentSubregions, variancesOfDifferentSubregions

# Can use agg_filters in matplotlib to do image manipulations; In this case, we can use this to add noise to clusters in IvsG plots based on their variance
# http://matplotlib.1069221.n5.nabble.com/Documentation-on-AGG-filters-td26791.html
# https: // matplotlib.org / devdocs / api / _as_gen / matplotlib.patches.Rectangle.html
# http://matplotlib.org/examples/pylab_examples/demo_agg_filter.html
# http://scikit-image.org/docs/stable/api/skimage.util.html#random-noise
def plotPostProcessingClusterVariance(variance):
    """
    def plotPostProcessingClusterVarianceSpecific(image, dpi):
        noiseVarianceRatio=3
        new_image = random_noise(image,mode='speckle', mean=0, var=noiseVarianceRatio*variance, clip=True)
	#print variance
        #new_image = random_noise(image,mode='pepper', amount =variance)
	return new_image,0,0
    return plotPostProcessingClusterVarianceSpecific
    """
    def post_processing(image, dpi):
	noiseVarianceRatio=2
        for i in range(int(image.shape[0]*image.shape[1]*variance*noiseVarianceRatio)):
            x, y = random.randint(1, image.shape[0] - 1), random.randint(1, image.shape[1] - 1)
            image[x, y, 0] = 0
            image[x, y, 1] = 0
            image[x, y, 2] = 0
        return image, 0, 0
    return post_processing


if __name__ == "__main__":
    i = nf.info();
    #3-cell
    networkSize=3;
    coupling_strengths_chlorine = 0.005 * np.asarray(np.matrix('0 1 1 ; 1 0 1 ; 1 1 0 '))
    n = netw.network(networkSize, info=i, coupling_strengths_chlorine=coupling_strengths_chlorine,
                                parameterSet=fh.parameters_generic(networkSize, [0.42, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.]));
    n.CYCLES = 100
    d, p, noP = sweepTrajectoriesGPU(network=n, sweepResolution=70)
    clusterize(d, minClusterSize=50, roundingDecimals=2)
