#!/usr/bin/env python

import sys

import Network_Generic_allSynapseTypes as netw


sys.path.insert(0, '../Tools')
import window as win
import fitzhugh_allSynapseTypes as model
import tools as tl
import numpy as np
import pylab as pl

class system(win.window):
    title = "System"
    figsize = (4.7*2, 4*2)
    debug = False

    def __init__(self, info=None, position=None, parameterArray = None, network=None, traces=None):
        win.window.__init__(self, position)

        self.dt = 0.1
        self.stride = 1
        self.info = info
        self.network = network
        self.traces = traces

        if parameterArray is None:
            self.parameterArray = model.parameters_generic(1)
        else :
            self.parameterArray = parameterArray

        """
        if self.network == None:
            self.parameterArray = model.parameters_generic(1)
        else:
            self.parameterArray = self.network.parameterSet[0:model.NP]
        """
        self.setupFig()

    def setupFig(self):
        self.fig.clear()
        self.x_orbit, self.y_orbit, self.orbit_period = None, None, None
        self.ax = self.fig.add_subplot(111, yticks=[-1.5, -0.5, 0.5, 1.5], xticks=[0, 0.5, 1.0])
        self.ax.set_xlim(-0.05, 1.05)
        self.x_min, self.x_max = -1.5, 1.5
        self.ax.set_ylim(self.x_min, self.x_max)
        self.ax.set_ylabel(r'Voltage $V$', fontsize=18)
        self.ax.set_xlabel(r'Recovery $x$', fontsize=18)
        self.ax.set_title('Single-Cell Dynamics (+coupling)', fontsize=18)

        self.li_traj, = self.ax.plot([], [], 'k-', lw=1., label='Oscillation Cycle')
        self.li_ncl_y, = self.ax.plot([], [], 'b--', lw=2., label='nullcline $\dot{V}=0$')
        self.li_ncl_x, = self.ax.plot([], [], 'g--', lw=2., label='nullcline $\dot{x}=0$')
        self.li_sft_ncl_x, = self.ax.plot([], [], 'r-.', lw=2., label='with coupling')
        self.tx_state_space = self.ax.text(0.2, -0.5, '', color='r')

        # pl.legend(loc=0, prop=dict(size=18))
        self.refresh_nullclines()
        self.refresh_orbit()

    def load_initial_condition(self, initialPhase=None ):
        X = np.zeros(model.N_EQ1, float)
        if(initialPhase==None):
            phase=tl.PI2*(pl.rand())
        else :
            phase=tl.PI2*(initialPhase%1)
            # Motiftoolbox code was computing 1.-initialStateArray[i] to account for the fact that in computing phase differences of traces, they're supplemented - two times negation to match the phase differences of traces :D
        X[0] = self.x_orbit(phase)
        X[1] = self.y_orbit(phase)
        return X

    def refresh_nullclines(self):
        x_i = np.arange(self.x_min, self.x_max, 0.01)

        nullcline_x = model.nullcline_x(x_i, self.parameterArray[0], self.parameterArray[7]) #model.params['I_0'], model.params['m_0'])
        nullcline_y = model.nullcline_y(x_i, self.parameterArray[2], self.parameterArray[3]) #model.params['x_0'], model.params['k_0'])

        xscale, yscale = 1., 3.
        nullcline_x, X_i = tl.adjustForPlotting(nullcline_x, x_i, ratio=xscale / yscale, threshold=0.03 * xscale)
        nullcline_y, x_i = tl.adjustForPlotting(nullcline_y, x_i, ratio=xscale / yscale, threshold=0.03 * xscale)

        self.li_ncl_x.set_data(nullcline_x, X_i)
        self.li_ncl_y.set_data(nullcline_y, x_i)

        if not self.network == None:
            self.li_sft_ncl_x.set_data([], [])

        self.fig.canvas.draw()


    def refresh_orbit(self):
        try:
            new_x, new_y, new_period = model.single_orbit(self.parameterArray, N_ORBIT=10 ** 4)
            self.x_orbit, self.y_orbit, self.orbit_period = new_x, new_y, new_period
            self.tx_state_space.set_text("")
        except:
            print "No closed orbit found!"
            self.tx_state_space.set_text("No closed orbit found!")
            raise ValueError

        phi = np.arange(500) / float(499.)

        xscale, yscale = 1., 3.
        y, x = tl.adjustForPlotting(self.y_orbit(tl.PI2 * phi), self.x_orbit(tl.PI2 * phi), ratio=xscale / yscale,
                                    threshold=0.03 * xscale)
        y[-1], x[-1] = y[0], x[0]

        self.li_traj.set_data(y, x)
        self.fig.canvas.draw()

    def N_output(self, CYCLES):
        return int(CYCLES * self.orbit_period / self.dt)

if __name__ == "__main__":
    import pylab as pl

    '''
    coupling_strengths_chlorine = 0.01 * np.asarray(np.matrix('0 1 1 0 0 0;1 0 1 0 0 0; 1 1 0 0 0 0; 0 0 0 0 1 1; 0 0 0 1 0 1; 0 0 0 1 1 0 '))
    coupling_strengths_electric = 0.01 * np.asarray(np.matrix('0 0 0 1 0 0;0 0 0 0 0 0; 0 0 0 0 0 0; 1 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0 '))
    n = netw.network(6, coupling_strengths_chlorine=coupling_strengths_chlorine, coupling_strengths_electric=coupling_strengths_electric,
                     parameterSet=fh.parameters_generic(6, [0.4, 0.5, 0., 10., -1, -1.2, 1.5, 1.]));
    '''

    n = netw.network(3, coupling_strengths_chlorine=0.01*np.asarray(np.matrix('0 1 1; 1 0 1; 1 1 0')),
                            parameterSet=np.array([0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                                                   0.4, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.,
                                                   0.5, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.]))
    s = system(network=n)
    print n.load_initial_condition_generic([0.5,0.5,0.5])
    pl.show()
