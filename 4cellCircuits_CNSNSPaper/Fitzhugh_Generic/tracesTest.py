import traces_allSynapses as tr
import Network_Generic_allSynapseTypes as netw
import numpy as np
import system_allSynapses as sys
import pylab as pl
import fitzhugh_allSynapseTypes as fh

networkSize = 3
cs = 0.005 * np.asarray(np.matrix("0 1 1 ; 1 0 1; 1 1 0 "))
#cs = 0.2 * np.asarray(np.matrix("0 1 1 ; 1 0 1; 1 1 0 ")) ## Chimera for very strong 'g'

parameterSet = fh.parameters_generic(networkSize, [0.43, 0.3, 0., 10., -1.5, -1.2, 1.5, 1.])

n = netw.network(networkSize, parameterSet=parameterSet, coupling_strengths_chlorine=cs)#, coupling_strengths_electric=cse)
tra = tr.traces(network=n)
n.CYCLES = 100

initialCondition = [0.,0.4,0.6]

tra.computeTraces(n.load_initial_condition_generic(initialCondition), printLast20=False, plotAnimation=True, noOfCyclesToAnimate=10, plotTransient=False, plotOrbit=True, noOfCyclesInOrbit=1)
pl.show()
