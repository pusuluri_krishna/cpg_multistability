
---------------------------------------------
INSTALL
---------------------------------------------
Tested with Anaconda Python 2.7 and PGCC V15.5

PGCC for accelerated computing with OpenAcc can be downloaded from https://developer.nvidia.com/openacc-toolkit

The following python packages are needed:

    sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose python-pip
    sudo pip install mpld3

---------------------------------------------
BUILD
---------------------------------------------

For single CPU setup:
    cd Tools/
    make all

For multicore CPU setup:
    cd Tools/
    make all -f makefile_cpu_multicore
    Need to set env variable ACC_NUM_CORES to specify number of CPU cores to use

For Nvidia GPU setup:
    cd Tools/
    make all -f makefile_gpu

---------------------------------------------
RUN
---------------------------------------------

cd Fitzhugh_Generic
python 4CellPaper.py

This would compute and generate the images in the following publication:
    Pusuluri, K., Basodi, S. and Shilnikov, A., 2019. Computational exposition of multistable rhythms in 4-cell neural circuits. Communications in Nonlinear Science and Numerical Simulation, p.105139.

---------------------------------------------
CONTRIBUTIONS AND ACKNOWLEDGEMENTS:
---------------------------------------------

This was initially forked from Motiftoolbox for 3-cell circuits (https://github.com/jusjusjus/Motiftoolbox), with continued independent development.

This work extends and generalizes the multistability analysis to larger networks, as well as varied synaptic inputs ( excitatory/inhibitory chemical/electrical) using Nvidia CUDA, directive based acceleration with OpenAcc, as well unsupervised machine learning in addition to the tools of dynamical systems analysis.

Contributors: Krishna Pusuluri, Sunitha Basodi