---------------------------------------------
CPG Multistability
---------------------------------------------

This repository can be used for performing multistability analysis of biological neural networks (central pattern generators) using a combination of dynamical systems analysis with Poincare return maps for phase lags and unsupervised machine learning (clustering). The code is presented for all the computational methods described and to generate the images in the following publications:

    Pusuluri, K., Basodi, S. and Shilnikov, A., 2019. Computational exposition of multistable rhythms in 4-cell neural circuits. Communications in Nonlinear Science and Numerical Simulation, p.105139.

If you make use of this repository in your research, please consider citing the above article.